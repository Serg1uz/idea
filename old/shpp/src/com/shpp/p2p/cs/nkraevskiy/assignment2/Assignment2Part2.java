package com.shpp.p2p.cs.nkraevskiy.assignment2;

import acm.graphics.GOval;
import acm.graphics.GRect;
import com.shpp.cs.a.graphics.WindowProgram;

import java.awt.*;

public class Assignment2Part2 extends WindowProgram {
//set scene parameters and ball diameter
    private static final int Diam = 100;
    public static final int APPLICATION_WIDTH = 300;
    public static final int APPLICATION_HEIGHT = 300;

    public void run() {
        createOvalsAndARectangle();
    }

    void createOvalsAndARectangle() {
        //Create the first oval and give it a color
        GOval firstOval = new GOval(0, 0, Diam, Diam);
        firstOval.setFilled(true);
        firstOval.setFillColor(Color.BLACK);
        add(firstOval);
        //Create a second oval and give it a color
        GOval secondOval = new GOval(0, getHeight() - Diam, Diam, Diam);
        secondOval.setFilled(true);
        secondOval.setFillColor(Color.BLACK);
        add(secondOval);
        //Create the third oval and give it a color
        GOval thirdOval = new GOval(getWidth() - Diam, 0, Diam, Diam);
        thirdOval.setFilled(true);
        thirdOval.setFillColor(Color.BLACK);
        add(thirdOval);
        //Create the fourth oval and give it a color
        GOval fourthOval = new GOval(getWidth() - Diam, getHeight() - Diam, Diam, Diam);
        fourthOval.setFilled(true);
        fourthOval.setFillColor(Color.BLACK);
        add(fourthOval);
        //Create a square in the middle of the ovals
        GRect  rectangle = new GRect(Diam / 2, Diam / 2, getWidth() / 1.5, getHeight() / 1.5);
        rectangle.setFilled(true);
        rectangle.setFillColor(Color.WHITE);
        rectangle.setColor(Color.WHITE);
        add(rectangle);
    }
}
