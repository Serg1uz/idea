package com.shpp.p2p.cs.nkraevskiy.assignment2;

import acm.graphics.GOval;
import com.shpp.cs.a.graphics.WindowProgram;

import java.awt.*;

public class Assignment2Part6 extends WindowProgram {
    /*
    Scene parameters
     */
    public static final int APPLICATION_WIDTH = 410;
    public static final int APPLICATION_HEIGHT = 160;
    /*
    Create a new color
    */
    public static final Color DARK_GREEN = new Color(20, 163, 1, 255);
    public static final Color DART_WEIDER = new Color(141, 1, 137);
    /*
    Number and width of ovals
     */
    private static final double NUM_OVALS = 6;
    private static final double OVAL_SIZE = 100;

    public void run() {
        createSnake();
    }

    /*
    Create an oval and set its color parameters
     */
    void createSnake() {
        for (int i = 0; i < NUM_OVALS; i++) {
            GOval snake = new GOval((OVAL_SIZE / 1.7) * i, 0, OVAL_SIZE, OVAL_SIZE);
            if (i % 2 == 0) snake.setLocation((OVAL_SIZE / 1.7) * i, OVAL_SIZE / 3);
            snake.setFilled(true);
            snake.setFillColor(DARK_GREEN);
            snake.setColor(DART_WEIDER);
            add(snake);
        }
    }
}
