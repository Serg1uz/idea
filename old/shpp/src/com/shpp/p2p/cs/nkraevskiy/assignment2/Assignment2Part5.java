package com.shpp.p2p.cs.nkraevskiy.assignment2;

import acm.graphics.GRect;
import com.shpp.cs.a.graphics.WindowProgram;

import java.awt.*;

public class Assignment2Part5 extends WindowProgram {
    /* The number of rows and columns in the grid, respectively. */
    public static final int APPLICATION_WIDTH = 500;
    public static final int APPLICATION_HEIGHT = 400;

    /* The number of rows and columns in the grid, respectively. */
    private static final double NUM_ROWS = 5;
    private static final double NUM_COLS = 6;

    /* The width and height of each box. */
    private static final double BOX_SIZE = 40;

    /* The horizontal and vertical spacing between the boxes. */
    private static final double BOX_SPACING = 20;
    private static final double MID_X = ((APPLICATION_WIDTH / 2) - (((BOX_SIZE+BOX_SPACING) / 1.6) * NUM_COLS));
    private static final double MID_Y = ((APPLICATION_HEIGHT / 2) - (((BOX_SIZE+BOX_SPACING) / 1.5) * NUM_ROWS));

    public void run() {
        createBox();
    }

    /*
    Performing a cycle of creating squares
     */
    void createBox() {
        for (int j = 0; j < NUM_ROWS; j++) {
            for (int i = 0; i < NUM_COLS; i++) {
                GRect box = new GRect(MID_X + i * (BOX_SIZE + BOX_SPACING), MID_Y + j * (BOX_SIZE + BOX_SPACING), BOX_SIZE, BOX_SIZE);
                box.setFilled(true); //Choosing a color and whether to fill
                box.setFillColor(Color.BLACK);
                add(box);
            }

        }
    }
}
