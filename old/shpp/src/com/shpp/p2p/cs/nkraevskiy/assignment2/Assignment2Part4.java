package com.shpp.p2p.cs.nkraevskiy.assignment2;

import acm.graphics.GLabel;
import acm.graphics.GRect;
import com.shpp.cs.a.graphics.WindowProgram;

import java.awt.*;

public class Assignment2Part4 extends WindowProgram {
    /*
    Scene parameters
     */
    public static final int APPLICATION_WIDTH = 600;
    public static final int APPLICATION_HEIGHT = 600;
    /*
    Create a new color
     */
    private static final Color FERN_GREEN = new Color(0, 146, 70);
    private static final Color BRIGHT_WHITE = new Color(241, 242, 241);
    private static final Color FLAME_SCARLET = new Color(206, 43, 55);
    /*
    Setting the flag parameters
     */
    private static final double RECTANGLE_X = 100;
    private static final double RECTANGLE_Y = 150;
    /*
    Setting the flag position
     */
    private static final double MID_X = ((APPLICATION_WIDTH / 2) - (RECTANGLE_X * 1.5));
    private static final double MID_Y = (((APPLICATION_HEIGHT - 26) / 2) - (RECTANGLE_Y / 1.5));
    private static final double SIZE_OF_THE_RECTANGLE = RECTANGLE_X;
    /*
    Setting the flag color
     */
    private static final boolean thisRectIsNotFilled = false;
    private static final boolean thisRectIsFilled = true;

    /*
    We call the flag with different parameters
    */
    public void run() {
        buildAFlag(FERN_GREEN, MID_X, SIZE_OF_THE_RECTANGLE, thisRectIsFilled);
        buildAFlag(BRIGHT_WHITE, MID_X + RECTANGLE_X, SIZE_OF_THE_RECTANGLE, thisRectIsFilled);
        buildAFlag(FLAME_SCARLET, MID_X + RECTANGLE_X * 2, SIZE_OF_THE_RECTANGLE, thisRectIsFilled);
        buildAFlag(Color.black, MID_X, SIZE_OF_THE_RECTANGLE * 3, thisRectIsNotFilled);
        text();
    }

    /**
     * @param colorOfRect      Color selection function
     * @param xPos             X flag position function
     * @param xSize            Flag size in width
     * @param isThisRectFilled Filling the flag
     */
    /*
    Create a flag and set parameters for it
     */
    private void buildAFlag(Color colorOfRect, double xPos, double xSize, boolean isThisRectFilled) {
        GRect firsRectangle = new GRect(xPos, MID_Y, xSize, SIZE_OF_THE_RECTANGLE * 2);
        firsRectangle.setFilled(isThisRectFilled);
        firsRectangle.setColor(colorOfRect);
        add(firsRectangle);
    }

    /*
    Create text
     */
    void text() {
        GLabel text = new GLabel("Flag of Italy", getWidth() - RECTANGLE_X * 1.7, getHeight() - RECTANGLE_Y / 3.5);
        text.setFont("Courier New-20");
        add(text);
    }
}
