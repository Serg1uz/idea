package com.shpp.p2p.cs.nkraevskiy.assignment2;

import com.shpp.cs.a.console.TextProgram;

public class Assignment2Part1 extends TextProgram {
    double a, b, c, x, y;

    public void run() { //performance of functions
        enterNumbers();
        discriminant();
    }
    /** лоодло */
    void enterNumbers() {
        a = readInt("Enter a integer: ");//Enter a
        b = readInt("Enter b integer: ");//Enter b
        c = readInt("Enter c integer: ");//Enter c
    }

    void discriminant() {
        double D, D1;
        D = Math.pow(b, 2) - 4 * a * c; //Calculating the discriminant
        D1 = Math.sqrt(D);//Extracting the root
        if (D > 0) { //If D is greater than 0, then we perform the calculation of two roots
            x = (-b + D1) / (2 * a);
            y = (-b - D1) / (2 * a);
            println("There are two roots: ");
            println("x= " + x);
            println("y= " + y);
        }
        if (D == 0) {//If D is 0, then we perform the calculation of one root
            x = -b / (2 * a);
            println("There is one root: x= " + x);
        } //In all other cases, we write that there are no roots
        else println("There are no real roots");
    }
}
