/*
 *  File: Assignment3part1.java
 *_____________________________
 * Solves task "Aerobics" :
 * calculate Cardiovacular health and Blood pressure
 *
 * A simple algorithm for solving the  task:
 * 1. get input data
 * 2. calc Cardiovacular health
 * 3. calc Blood pressure
 *
 * Author: Chernov Sergey 10/13/20
 */
package com.shpp.p2p.cs.schernov.assignmet3;


import com.shpp.cs.a.console.TextProgram;

import java.util.ArrayList;
import java.util.List;

public class Assignment3part1 extends TextProgram {
    //constants must minutes for aerobics
    final static int CARDIOVACULAR_MINUTES = 30;
    final static int CARDIOVACULAR_DAYS = 5;
    final static int BLOOD_PRESSURE_MINUTES = 40;
    final static int BLOOD_PRESSURE_DAYS = 3;

    //global array minute get aerobics
    List<Integer> minutes = new ArrayList<>();

    /**
     * Specifies the program entry point
     */
    public void run() {
        getInputData();
        calcCardiovacularHealth();
        calcBloodPressure();
    }

    /**
     * The method getInputData
     * get input data from screen to array minutes
     * <p>
     * in loop get count of minute sport time in day
     */
    private void getInputData() {
        for (int i = 0; i < 7; i++) {
            minutes.add(readInt("How many minutes did you do on day " + (i + 1) + "?"));
        }
    }

    /**
     * The method calcCardiovacularHealth
     * calculated result Cardiovacular Health
     * using algorithm
     * 1. calculated count aerobics days by params Minutes
     * 2. equals need days with count days
     * 3. show result
     */
    private void calcCardiovacularHealth() {
        println("Cardiovacular health:");
        int countDays = calcAerobicsDay(CARDIOVACULAR_MINUTES);
        if (countDays >= CARDIOVACULAR_DAYS) {
            println("  Great job! You've done enough exercise for cardiovacular health.");
        } else {
            println("  You needed to train hard for at least " + (CARDIOVACULAR_DAYS - countDays) + " more day(s) a week!.");
        }
    }

    /**
     * The method calcBloodPressure
     * calculated result Blood pressure
     * using algorithm
     * 1. calculated count aerobics days by params Minutes
     * 2. equals need days with count days
     * 3. show result
     */
    private void calcBloodPressure() {
        println("Blood Pressure:");
        int countDays = calcAerobicsDay(BLOOD_PRESSURE_MINUTES);
        if (countDays >= BLOOD_PRESSURE_DAYS) {
            println("  Great job! You've done enough exercise to keep a low blood pressure.");
        } else {
            println("  You needed to train hard for at least " + (BLOOD_PRESSURE_DAYS - countDays) + " more day(s) a week!.");
        }
    }

    /**
     * The method calcAerobicsDays
     * calculated total aerobics day from week
     * by equals need minutes
     *
     * @param minutes int need equal minutes
     * @return int count of day when aerobics time >= need minutes
     */
    private int calcAerobicsDay(int minutes) {
        int result = 0;
        for (int i : this.minutes) {
            if (i >= minutes) {
                result++;
            }
        }
        return result;
    }
}
