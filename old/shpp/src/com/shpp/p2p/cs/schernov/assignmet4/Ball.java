/*
 *  File: Ball.java
 *_____________________________
 * Solves task "" :
 *
 *
 * A simple algorithm for solving the  task:
 *
 * Author: Chernov Sergey 10/20/20
 */

package com.shpp.p2p.cs.schernov.assignmet4;


import acm.graphics.GOval;

import java.awt.*;

public class Ball extends GOval {
    final Board board;
    final Paddle paddle;
    final double radius;
    double vX;
    double vY;
    boolean inGame;

    public Ball(double x, double y, double radius, Color color, Board board, Paddle paddle) {
        super(x, y, radius, radius);
        this.setColor(color);
        this.setFilled(true);
        this.board = board;
        this.vX = 3;
        this.vY = 3;
        this.radius = radius;
        this.paddle = paddle;
        this.inGame = false;
    }

    public void calcSpeedBall() {
        if ((this.getX() - vX <= board.left && vX < 0) || (this.getX() + vX >= board.right - radius * 2 && vX > 0)) {
            vX = -vX;
        }

        if (this.getY() - vY <= board.top && vY < 0) {
            vY = -vY;
        }

        if (this.getY() >= paddle.y + radius / 2) {
            this.inGame = false;
        }
    }

    public void moved() {
        this.move(vX, vY);
    }

}

