/*
 *  File: Assignment3part4.java
 *_____________________________
 * Solves task "Pyramid" :
 * you will need to throw a pyramid of bricks.
 * Each row contains 1 brick less.
 * The pyramid should be centered horizontally and lie on the "bottom" of the window.
 *
 * A simple algorithm for solving the  task:
 * using double recursive draw many bricks
 * or show error message if pyramid size more the screen size
 *
 * Author: Chernov Sergey 10/13/20
 */

package com.shpp.p2p.cs.schernov.assignmet3;

import acm.graphics.GLabel;
import acm.graphics.GRect;
import com.shpp.cs.a.graphics.WindowProgram;

import java.awt.*;

public class Assignment3part4 extends WindowProgram {
    //constant for task
    private static final double BRICK_HEIGHT = 20;
    private static final double BRICK_WIDTH = 40;
    private static final int BRICKS_IN_BASE = 15;

    //variables start position
    double startX, startY;

    /**
     * Specifies the program entry point
     */
    public void run() {
        calcStartPosition();
        if (checkSize()) {
            drawPyramid(startX, startY, BRICKS_IN_BASE);
        } else {
            showError();
        }
    }

    /**
     * The method calcStartPosition
     * calculated start XY-coordinate for first brick in our pyramid
     * X-Coordinate using formula centered picture
     * Y-Coordinate calculated by height screen - height bricks
     */
    private void calcStartPosition() {
        this.startX = (getWidth() - BRICK_WIDTH * BRICKS_IN_BASE) / 2;
        this.startY = getHeight() - BRICK_HEIGHT;
    }

    /**
     * The method checkSize
     * checks if the pyramid fits the screen size
     *
     * @return Boolean
     */
    private boolean checkSize() {
        return getWidth() > BRICK_WIDTH * BRICKS_IN_BASE && getHeight() > BRICK_HEIGHT * BRICKS_IN_BASE;
    }

    /**
     * The method drawPyramid
     * first recursive method when draw line from bricks
     * calculated new start XY-Coordinate
     * Exit from recursion when count of bricks=0
     *
     * @param startX double start X-Coordinate  line
     * @param startY double start Y-Coordinate  line
     * @param bricks int count bricks in line
     */
    private void drawPyramid(double startX, double startY, int bricks) {
        if (bricks > 0) {
            drawLine(startX, startY, bricks);
            startX += BRICK_WIDTH / 2;
            startY -= BRICK_HEIGHT;
            drawPyramid(startX, startY, bricks - 1);
        }
    }

    /**
     * The method drawLine
     * second recursive method when draw bricks in line
     * calculated new start XY-Coordinate
     * Exit from recursion when count of bricks=0
     *
     * @param startX double start X-Coordinate brick in line
     * @param startY double start Y-Coordinate brick in line
     * @param bricks int count bricks in line
     */
    private void drawLine(double startX, double startY, int bricks) {
        if (bricks > 0) {
            drawRectangle(startX, startY);
            startX += BRICK_WIDTH;
            drawLine(startX, startY, bricks - 1);
        }
    }

    /**
     * The method drawRectangle
     * draw rectangle aka brick from XY-coordinate
     * width and height bricks is constants
     * color fill used orange
     * color border used black
     *
     * @param x double start X-Coordinate rectangle
     * @param y double start Y-Coordinate rectangle
     */
    private void drawRectangle(double x, double y) {
        GRect rect = new GRect(x, y, BRICK_WIDTH, BRICK_HEIGHT);
        rect.setFillColor(Color.orange);
        rect.setFilled(true);
        rect.setColor(Color.black);
        add(rect);
    }

    /**
     * The method showError
     * draw error message into center screen
     */
    private void showError() {
        GLabel error = new GLabel("Error: Too many or too large bricks. " +
                "The picture does not fit on the screen!");
        error.setFont("Helvetica-18");
        error.setColor(Color.red);
        double x = (getWidth() - error.getWidth()) / 2;
        double y = (getHeight() - error.getHeight()) / 2;
        add(error, x, y);
    }

}
