/*
 * File: Assignment2part5.java
 *
 * Solves task "Illusion: draw many squares" :
 * we must draw many black squares with spacing
 *
 * A simple algorithm for solving the  task:
 * 1. calculated main picture size
 * 2. centered position
 * 4. draw main black rectangle
 * 5. draw spacing white rectangle
 */

package com.shpp.p2p.cs.schernov.assignmet2;

import acm.graphics.GLabel;
import acm.graphics.GRect;
import com.shpp.cs.a.graphics.WindowProgram;

import java.awt.*;


public class Assignment2part5 extends WindowProgram {
    /* The number of rows and columns in the grid, respectively. */
    private static final int NUM_ROWS = 5;
    private static final int NUM_COLS = 6;

    /* The width and height of each box. */
    private static final double BOX_SIZE = 40;

    /* The horizontal and vertical spacing between the boxes. */
    private static final double BOX_SPACING = 10;

    double widthRectangle, heightRectangle;
    double startX, startY;

    public void run() {
        //get our picture size
        calcSizePicture();
        //get centered
        getStartPosition();
        //draw main picture
        drawMainPicture();
    }

    /**
     * The method calcSizePicture
     * calculated main picture size by algorithm
     * box size * count box + spacing size * (count box - 1)
     */
    private void calcSizePicture() {
        this.widthRectangle = BOX_SIZE * NUM_COLS + BOX_SPACING * (NUM_COLS - 1);
        this.heightRectangle = BOX_SIZE * NUM_ROWS + BOX_SPACING * (NUM_ROWS - 1);
    }

    /**
     * The method getStartPosition
     * centered picture by calculated main picture start position
     * using algorithm
     * (size window - size picture) / 2
     */
    private void getStartPosition() {
        this.startX = (getWidth() - this.widthRectangle) / 2;
        this.startY = (getHeight() - this.heightRectangle) / 2;
    }

    /**
     * The method drawMainPicture
     * draw main picture:
     * 1. draw one big black rectangle
     * 2. draw horizontal and vertical spacing rectangle
     * with check size window with size picture
     * if size picture more window draw error label
     */
    private void drawMainPicture() {
        if (this.widthRectangle <= getWidth() && this.heightRectangle <= getHeight()) {
            //draw main Rectangle
            drawRectangle(startX, startY, widthRectangle, heightRectangle, Color.black);
            //draw Vertical Spacing
            drawVerticalSpacing(startX, startY, heightRectangle);
            //draw Horizontal Spacing
            drawHorizontalSpacing(startX, startY, widthRectangle);
        } else {
            //show error when main picture size more than windows size
            GLabel error = new GLabel("Error: Rectangle picture more than windows size");
            error.setFont("Helvetica-18");
            error.setColor(Color.red);
            add(error, 20, 20);
        }
    }

    /**
     * The method drawHorizontalSpacing
     * draw horizontal spacing line
     *
     * @param startX         double start X-coordinate
     * @param startY         double start Y-coordinate
     * @param widthRectangle double width of horizontal spacing rectangle
     **/
    private void drawHorizontalSpacing(double startX, double startY, double widthRectangle) {
        double tempY;
        for (int i = 1; i < (NUM_ROWS); i++) {
            tempY = startY + BOX_SIZE * i + BOX_SPACING * (i - 1);
            drawRectangle(startX, tempY, widthRectangle, BOX_SPACING, Color.white);
        }
    }

    /**
     * The method drawVerticalSpacing
     * draw vertical spacing line
     *
     * @param startX          double start X-coordinate
     * @param startY          double start Y-coordinate
     * @param heightRectangle double height of vertical spacing rectangle
     **/
    private void drawVerticalSpacing(double startX, double startY, double heightRectangle) {
        double tempX;
        for (int i = 1; i < (NUM_COLS); i++) {
            tempX = startX + BOX_SIZE * i + BOX_SPACING * (i - 1);
            drawRectangle(tempX, startY, BOX_SPACING, heightRectangle, Color.white);
        }
    }

    /**
     * The method draw rectangle
     *
     * @param x               start X-Coordinate
     * @param y               start Y-Coordinate
     * @param widthRectangle  rectangle width
     * @param heightRectangle rectangle height
     * @param color           rectangle color
     */
    private void drawRectangle(double x, double y, double widthRectangle, double heightRectangle, Color color) {
        GRect rect = new GRect(x, y, widthRectangle, heightRectangle);
        rect.setFilled(true);
        rect.setColor(color);
        add(rect);
    }


}