/*
 * File: Assignment2Part1.java
 *
 * Solves task "solution of a quadratic equation" :
 *
 * A simple algorithm for solving the  task:
 * 1. get incoming data
 * 2. solve quadratic equation
 * 3. output the result
 */

package com.shpp.p2p.cs.schernov.assignmet2;

import com.shpp.cs.a.console.TextProgram;

import java.util.ArrayList;
import java.util.List;

class QuadraticEquation extends TextProgram {
    /**
     * Simple class for calculated result for
     * quadratic equation as ax^2+bx+c=0
     */

    //global variables: params quadratic equation by formula ax^2+bx+c=0;
    double a, b, c;

    //global variables: calculated discriminant
    double discriminant;
    //global variables: calculated results
    List<Double> results = new ArrayList<Double>();


    /**
     * Specifies the class entry point
     *
     * @param a Double: first param of quadratic equation
     * @param b Double: second param of quadratic equation
     * @param c Double: thirds param of quadratic equation
     */
    public QuadraticEquation(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    /**
     * Class method for calculate result quadratic equation
     * 1. calculated discriminant
     * 2. calculated results
     */
    public void getResult() {
        this.calcDiscriminant();
        this.calcResultEquation();
    }

    /**
     * set global variables discriminant
     * calculated by formula b^2-4ac
     */
    private void calcDiscriminant() {
        this.discriminant = b * b - 4 * a * c;
    }

    /**
     * set global variables results (arraylist)
     * depending on the existing discriminant
     * 1. less 0 - no result
     * 2. equal 0 - 1 result
     * 3. more 0 - 2 results
     */
    private void calcResultEquation() {
        if (this.discriminant < 0) {
            //if discriminant less 0 then equation not have real result
            this.results = null;
        } else if (this.discriminant == 0) {
            //if discriminant equal 0 then equation have one real result
            double x = (-b) / (2 * a);
            this.results.add(x);
        } else {
            //if discriminant more 0 then equation have two real results
            double x1 = (-b + Math.sqrt(discriminant)) / (2 * a);
            double x2 = (-b - Math.sqrt(discriminant)) / (2 * a);
            this.results.add(x1);
            this.results.add(x2);
        }
    }
}

public class Assignment2part1 extends TextProgram {
    // Quadratic equals variable from class
    QuadraticEquation quadraticEquation;

    /**
     * Specifies the program entry point
     */
    public void run() {
        getIncomingData();
        //Calculated result
        calculatedResults();
        //show result
        showResults();
    }

    /**
     * The method where we get incoming data for our
     * quadratic equation:
     * 1. from screen we get params a,b,c (double)
     * 2. init global variable from class QuadraticEquation
     */
    private void getIncomingData() {
        //incoming data
        double a = 0;

        while (a == 0) {
            println("First element can't be 0");
            a = readDouble("Please enter a: ");
        }

        double b = readDouble("Please enter b: ");
        double c = readDouble("Please enter c: ");
        //initial variable quadraticEquation
        this.quadraticEquation = new QuadraticEquation(a, b, c);
    }

    /**
     * The method call class method
     * for calculated result our quadratic equation
     */
    private void calculatedResults() {
        this.quadraticEquation.getResult();
    }

    /**
     * The method show result our quadratic equation
     * by choice count of result:
     * null - no real root
     * 1 - result have one real root
     * 2 and more- result have two real roots
     */
    private void showResults() {
        if (this.quadraticEquation.results != null && !this.quadraticEquation.results.isEmpty()) {
            if (this.quadraticEquation.results.size() == 1) {
                println("There is one root " + this.quadraticEquation.results.get(0));
            } else {
                println("There are two roots:" + this.quadraticEquation.results.get(0) + " and " + this.quadraticEquation.results.get(1));
            }
        } else {
            println("There are no real roots");
        }
    }
}