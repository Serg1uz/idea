/*
 * File: Assignment2part2.java
 *
 * Solves task "Illusion: rectangle on the circle" :
 *
 * A simple algorithm for solving the  task:
 * 1. Calculate limit size and random radius for 4 circle
 * 2. Draw 4 circles into corner screen
 * 3. Draw rectangle on the circles
 */

package com.shpp.p2p.cs.schernov.assignmet2;

import acm.graphics.GOval;
import acm.graphics.GRect;
import com.shpp.cs.a.graphics.WindowProgram;

import java.awt.*;
import java.util.Random;


public class Assignment2part2 extends WindowProgram {
    //global constants limit of screen
    public static final int APPLICATION_WIDTH = 300;
    public static final int APPLICATION_HEIGHT = 300;

    /**
     * Specifies the program entry point
     */
    public void run() {
        //calculate limit for size Circle
        double limit = getLimitSize();
        //get random radius for Circle
        double randomRadius = getRandomRadius(limit);
        //draw 4 Circle
        drawCircles(randomRadius);
        //draw Rectangle
        drawRectangle(randomRadius);
    }

    /**
     * The method calculate limit size of circle
     * by get fourth part of less size window
     *
     * @return double Max radius one circle
     * so that 4 circles fit without overlapping themselves
     */
    private double getLimitSize() {
        return getWidth() < getHeight() ? getWidth() / 4.0 : getHeight() / 4.0;
    }


    /**
     * The method draw white rectangle
     * when start XY-coordinate is center up-left circle
     * and size is difference from size window and double radius circle
     *
     * @param randomRadius basic radius circle
     */
    private void drawRectangle(double randomRadius) {
        double x = 0 + randomRadius;
        double y = 0 + randomRadius;
        double width = getWidth() - 2 * randomRadius;
        double height = getHeight() - 2 * randomRadius;
        GRect rect = new GRect(x, y, width, height);
        rect.setFilled(true);
        rect.setColor(Color.white);
        add(rect);
    }

    /**
     * The method calculated random Radius for circle from 20 to limit parameter
     *
     * @param limit max radius for windows size
     * @return double random radius for circle
     */
    private double getRandomRadius(double limit) {
        double begin = 20;
        double random = new Random().nextDouble();
        return begin + (random * (limit - begin));
    }

    /**
     * The method draw 4 circle into window corners
     *
     * @param randomRadius default radius for circle
     */
    private void drawCircles(double randomRadius) {
        //First
        drawCircle(0, 0, randomRadius);
        //Second
        drawCircle(getWidth() - 2 * randomRadius, 0, randomRadius);
        //Third
        drawCircle(getWidth() - 2 * randomRadius, getHeight() - 2 * randomRadius, randomRadius);
        //Fourth
        drawCircle(0, getHeight() - 2 * randomRadius, randomRadius);
    }

    /**
     * The method draw black circle by coordinate and width radius
     *
     * @param x      start X-Coordinate
     * @param y      start Y-Coordinate
     * @param radius radius Circle
     */
    private void drawCircle(double x, double y, double radius) {
        GOval circle = new GOval(x, y, 2 * radius, 2 * radius);
        circle.setColor(Color.black);
        circle.setFilled(true);
        add(circle);
    }
}