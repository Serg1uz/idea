/*
 * File: Assigment2Part4
 *
 * Solves task "Draw Flag" :
 * we must draw three-color flag center by windows
 *
 * A simple algorithm for solving the  task:
 * 0. description class object of flag
 * 1. generated array of flags
 * 2. calculated size of flag
 * 3. centered position flag
 * 4. get random flag from array
 * 5. draw flag and show label with name
 */

package com.shpp.p2p.cs.schernov.assignmet2;

import acm.graphics.GLabel;
import acm.graphics.GRect;
import com.shpp.cs.a.graphics.WindowProgram;

import java.awt.*;

/**
 * Class of description object Flag of country
 */
class Flag {

    public String name;
    public Boolean isHorizontal;
    public Color firstLine;
    public Color secondLine;
    public Color thirdLine;

    /**
     * Initial object Flag
     *
     * @param name         name of country String
     * @param isHorizontal line in flag have horizontal or vertical Boolean
     * @param firstLine    color first line Color
     * @param secondLine   color second line Color
     * @param thirdLine    color third line Color
     */
    public Flag(String name, Boolean isHorizontal, Color firstLine, Color secondLine, Color thirdLine) {
        this.name = name;
        this.isHorizontal = isHorizontal;
        this.firstLine = firstLine;
        this.secondLine = secondLine;
        this.thirdLine = thirdLine;
    }
}

public class Assignment2part4 extends WindowProgram {
    //constants windows size
    public static final int APPLICATION_WIDTH = 400;
    public static final int APPLICATION_HEIGHT = 400;
    //constants proportional size need values between 0.01 and 0.55
    public static final double FLAG_PROPORTIONAL = 0.55;
    //global variables start position
    double startX;
    double startY;
    //global variables size flag
    double widthFlag;
    double heightFlag;
    //Array of flags
    Flag[] flags = new Flag[10];

    /**
     * Specifies the program entry point
     */
    public void run() {
        //generate array of flags
        generatedFlags();
        //get Flag size used 3x5 proportional
        calcFlagSize();
        //get centered flag
        this.startX = (getWidth() - widthFlag) / 2;
        this.startY = (getHeight() - heightFlag) / 2;
        //get Random index for flags
        int randomFlagsIndex = (int) (Math.random() * flags.length);
        //Draw Flags
        drawFlag(flags[randomFlagsIndex]);

    }

    /**
     * The method calc size of flag
     * calculating 75% of the smallest side and equalizing
     * the size of the flag in proportion to 3x5
     */
    private void calcFlagSize() {
        if (getWidth() <= getHeight()) {
            this.widthFlag = getWidth() * FLAG_PROPORTIONAL;
            this.heightFlag = widthFlag / 5 * 3;
        } else {
            this.heightFlag = getHeight() * FLAG_PROPORTIONAL;
            this.widthFlag = heightFlag / 3 * 5;
        }
    }

    /**
     * The method for fill array of Flag
     * used data from wikipedia
     * set name, directions line
     **/
    private void generatedFlags() {
        flags[0] = new Flag("Armenia", true, Color.red, Color.blue, Color.yellow);
        flags[1] = new Flag("Austria", true, Color.red, Color.white, Color.red);
        flags[2] = new Flag("Belgium", false, Color.black, Color.yellow, Color.red);
        flags[3] = new Flag("Bulgaria", true, Color.white, Color.green, Color.red);
        flags[4] = new Flag("Chad", false, Color.blue, Color.yellow, Color.red);
        flags[5] = new Flag("France", false, Color.blue, Color.white, Color.red);
        flags[6] = new Flag("Gabon", true, Color.green, Color.yellow, Color.blue);
        flags[7] = new Flag("Germany", true, Color.black, Color.red, Color.yellow);
        flags[8] = new Flag("Ireland", false, Color.green, Color.white, Color.yellow);
        flags[9] = new Flag("Italy", false, Color.green, Color.white, Color.red);

    }

    /**
     * The method draw flag
     * get data from object Flag
     * draw three rectangle
     *
     * @param flag object Flag
     */
    private void drawFlag(Flag flag) {
        if (flag.isHorizontal) {
            //if flag have horizontal line
            drawRectangle(startX, startY, widthFlag, heightFlag / 3, flag.firstLine, true);
            drawRectangle(startX, startY + heightFlag / 3, widthFlag, heightFlag / 3, flag.secondLine, true);
            drawRectangle(startX, startY + 2 * heightFlag / 3, widthFlag, heightFlag / 3, flag.thirdLine, true);
        } else {
            //if flag have vertical line
            drawRectangle(startX, startY, widthFlag / 3, heightFlag, flag.firstLine, true);
            drawRectangle(startX + widthFlag / 3, startY, widthFlag / 3, heightFlag, flag.secondLine, true);
            drawRectangle(startX + 2 * widthFlag / 3, startY, widthFlag / 3, heightFlag, flag.thirdLine, true);
        }
        //draw border
        drawRectangle(startX, startY, widthFlag, heightFlag, Color.black, false);
        //draw label
        drawName(flag);
    }

    /**
     * The method draw label for flag
     * copied method from book about java
     *
     * @param flag object Flag
     */
    private void drawName(Flag flag) {
        GLabel flagName = new GLabel("Flag of " + flag.name);
        flagName.setFont("Helvetica-18");
        double x = getWidth() - flagName.getWidth() - 5;
        double y = getHeight() - 5;
        add(flagName, x, y);
    }

    /**
     * The method draw rectangle
     *
     * @param x               start X-Coordinate
     * @param y               start Y-Coordinate
     * @param widthRectangle  rectangle width
     * @param heightRectangle rectangle height
     * @param color           color
     * @param fill            rectangle is fill color
     */
    private void drawRectangle(double x, double y, double widthRectangle, double heightRectangle, Color color, Boolean fill) {
        GRect rect = new GRect(x, y, widthRectangle, heightRectangle);
        rect.setFilled(fill);
        rect.setColor(color);
        add(rect);
    }


}