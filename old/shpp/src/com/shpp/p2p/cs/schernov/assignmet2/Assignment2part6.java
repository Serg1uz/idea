/*
 * File: Assigment1Part1
 *
 * Solves task "Collect newspaper" :
 * Karel must leave the house and pick up a newspaper on the doorstep
 *
 * A simple algorithm for solving the  task:
 * 1. Mark start point
 * 2. Found Newspaper
 * 3. Return to start position
 *
 * Rule for the house(board):
 * 1. House(Board) have one door
 * 2. Newspaper one move from the house(board)
 */

package com.shpp.p2p.cs.schernov.assignmet2;

import acm.graphics.GOval;
import com.shpp.cs.a.graphics.WindowProgram;

import java.awt.*;


public class Assignment2part6 extends WindowProgram {
    public static final int APPLICATION_WIDTH = 400;
    public static final int APPLICATION_HEIGHT = 200;

    //count of part caterpillar
    public static final int COUNT_PARTS = 6;

    public void run() {
        //get radius for part caterpillar
        double radius = getRadius();
        //
        drawCaterpillar(radius);
    }

    /**
     * The method draw caterpillar
     *
     * @param radius radius one of the part caterpillar
     */
    private void drawCaterpillar(double radius) {
        double startX;
        double startY;
        for (int i = 0; i < COUNT_PARTS; i++) {
            startX = 2 * radius * 0.65 * i;
            if (COUNT_PARTS % 2 == 0) {
                startY = (i % 2 == 0) ? getHeight() - 2 * radius
                        : (getHeight() - 4 * radius * 0.65) < 0 ?
                        0 : getHeight() - 4 * radius * 0.65;
            } else {
                startY = (i % 2 != 0) ? getHeight() - 2 * radius
                        : (getHeight() - 4 * radius * 0.65) < 0 ?
                        0 : getHeight() - 4 * radius * 0.65;
            }
            drawCircle(startX, startY, radius);
        }
    }

    /**
     * The method calculated by optimize radius of caterpillar's part
     **/
    private double getRadius() {
        double diameter = APPLICATION_WIDTH / COUNT_PARTS * 1.45;
        diameter *= 0.85;
        while (diameter > getHeight() * 0.65) {
            diameter = diameter * 0.9;
        }
        return diameter / 2;
    }

    /**
     * The method draw circle
     *
     * @param x      start X-Coordinate
     * @param y      start Y-Coordinate
     * @param radius radius circle
     */
    public void drawCircle(double x, double y, double radius) {
        GOval circle = new GOval(x, y, 2 * radius, 2 * radius);
        circle.setColor(Color.red);
        circle.setFilled(true);
        circle.setFillColor(Color.green);
        add(circle);
    }


}