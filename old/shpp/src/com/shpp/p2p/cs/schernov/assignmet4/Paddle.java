/*
 *  File: Paddle.java
 *_____________________________
 * Solves task "" :
 *
 *
 * A simple algorithm for solving the  task:
 *
 * Author: Chernov Sergey 10/20/20
 */

package com.shpp.p2p.cs.schernov.assignmet4;

import acm.graphics.GRect;

import java.awt.*;
//import java.awt.event.*;

public class Paddle extends GRect {
    final double width;
    final double height;
    double x;
    final double y;

    final Color color;

    final Board board;

    public Paddle(double x, double y, double width, double height, Color color, Board board) {
        super(x, y, width, height);
        this.width = width;
        this.height = height;
        this.y = y;
        this.color = color;
        this.setFilled(true);
        this.setColor(color);
        this.board = board;
    }

    public void setPosition(double x) {
        if (x < board.left) {
            this.setLocation(Board.BOARD_LINE, y);
            this.setColor(Color.DARK_GRAY);
            board.setColors(Color.DARK_GRAY);
        } else if (x > board.right - width / 2 - 5) {
            this.setLocation(board.right - width, y);
            this.setColor(Color.DARK_GRAY);
            board.setColors(Color.DARK_GRAY);
        } else {
            this.setLocation(x, y);
            this.setColor(Color.BLACK);
            board.setColors(Color.BLACK);
        }
    }
}
