/*
 *  File: Board.java
 *_____________________________
 * Solves task "" :
 *
 *
 * A simple algorithm for solving the  task:
 *
 * Author: Chernov Sergey 10/20/20
 */

package com.shpp.p2p.cs.schernov.assignmet4;

import acm.graphics.GRect;
import org.jetbrains.annotations.NotNull;

import java.awt.*;

public class Board {
    public static final int BOARD_LINE = 5;
    public static final int INFO_BLOCK = 30;

    final double left;
    final double right;
    final double top;
    final double bottom;
    final @NotNull GRect leftLine;
    final @NotNull GRect rightLine;
    final @NotNull GRect topLine;
    final @NotNull GRect bottomLine;
    final @NotNull GRect scoreBlock;
    final @NotNull GRect bricksBlock;
    final @NotNull GRect liveBlock;
//    GOval[] lives = new GOval[3];

    public Board(double width, double height) {
        this.left = BOARD_LINE;
        this.top = BOARD_LINE + INFO_BLOCK;
        this.right = width - BOARD_LINE;
        this.bottom = height - BOARD_LINE;

        this.leftLine = new GRect(0, this.top, BOARD_LINE, height - this.top);
        leftLine.setColor(Color.BLACK);
        leftLine.setFilled(true);
        this.topLine = new GRect(0, this.top, width, BOARD_LINE);
        topLine.setColor(Color.BLACK);
        topLine.setFilled(true);
        this.rightLine = new GRect(right, this.top, BOARD_LINE, height - this.top);
        rightLine.setColor(Color.BLACK);
        rightLine.setFilled(true);
        this.bottomLine = new GRect(0, bottom, width, BOARD_LINE);
        bottomLine.setColor(Color.BLACK);
        bottomLine.setFilled(true);
        this.scoreBlock = new GRect(0, 0, width / 3.0, INFO_BLOCK);
        scoreBlock.setColor(Color.BLACK);
        scoreBlock.setFilled(false);
        this.bricksBlock = new GRect(scoreBlock.getX() + scoreBlock.getWidth(), 0, width / 3.0, INFO_BLOCK);
        bricksBlock.setColor(Color.BLACK);
        bricksBlock.setFilled(false);
        this.liveBlock = new GRect(bricksBlock.getX() + bricksBlock.getWidth(), 0, width / 3.0, INFO_BLOCK);
        liveBlock.setColor(Color.BLACK);
        liveBlock.setFilled(false);

    }

    public void setColors(Color color) {
        leftLine.setColor(color);
        topLine.setColor(color);
        rightLine.setColor(color);
        bottomLine.setColor(color);
    }

}
