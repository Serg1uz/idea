/*
 *  File: Brick.java
 *_____________________________
 * Solves task "" :
 *
 *
 * A simple algorithm for solving the  task:
 *
 * Author: Chernov Sergey 10/20/20
 */

package com.shpp.p2p.cs.schernov.assignmet4;

import acm.graphics.GRect;

import java.awt.*;

public class Brick extends GRect {
    final int row;
    final int column;
    final boolean isBrick = true;

    public Brick(double x, double y, double width, double height, Color color, int row, int column) {
        super(x, y, width, height);
        this.setColor(color);
        this.setFilled(true);
        this.row = row;
        this.column = column;
    }

    public int getScore() {
        if (this.getColor() == Color.cyan) return 1;
        if (this.getColor() == Color.green) return 2;
        if (this.getColor() == Color.yellow) return 3;
        if (this.getColor() == Color.orange) return 4;
        if (this.getColor() == Color.red) return 5;
        return 0;
    }

}
