/*
 *  File: Breakout.java
 *_____________________________
 * Solves task "Breakout" :
 * Game BreakOut
 *
 * A simple algorithm for solving the  task:
 *
 * Author: Chernov Sergey 10/20/20
 */

package com.shpp.p2p.cs.schernov.assignmet4;

import acm.graphics.GLabel;
import acm.graphics.GObject;
import acm.graphics.GOval;
import com.shpp.cs.a.graphics.WindowProgram;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.awt.*;
import java.awt.event.MouseEvent;


public class Breakout extends WindowProgram {
    /**
     * Width and height of application window in pixels
     */
    public static final int APPLICATION_WIDTH = 400;
    public static final int APPLICATION_HEIGHT = 600;

    /**
     * Dimensions of game board (usually the same)
     */
    private static final int WIDTH = APPLICATION_WIDTH;
    private static final int HEIGHT = APPLICATION_HEIGHT;

    /**
     * Dimensions of the paddle
     */
    private static final int PADDLE_WIDTH = 60;
    private static final int PADDLE_HEIGHT = 10;

    /**
     * Offset of the paddle up from the bottom
     */
    private static final int PADDLE_Y_OFFSET = 30;

    /**
     * Number of bricks per row
     */
    private static final int NBRICKS_PER_ROW = 10;

    /**
     * Number of rows of bricks
     */
    private static final int NBRICK_ROWS = 1;

    /**
     * Separation between bricks
     */
    private static final int BRICK_SEP = 4;

    /**
     * Width of a brick
     */
    private static final double BRICK_WIDTH =
            (WIDTH - (NBRICKS_PER_ROW - 1) * BRICK_SEP) / NBRICKS_PER_ROW - 0.5;

    /**
     * Height of a brick
     */
    private static final int BRICK_HEIGHT = 8;

    /**
     * Radius of the ball in pixels
     */
    private static final int BALL_RADIUS = 10;

    /**
     * Offset of the top brick row from the top
     */
    private static final int BRICK_Y_OFFSET = 70;

    /**
     * Number of turns
     */
    private static final int NTURNS = 3;

    Paddle paddle;
    double paddleYCoord;

    Ball ball;
    double ballX, ballY;

    Board board;

    int countBricks = NBRICK_ROWS * NBRICKS_PER_ROW;
    int score = 0;
    int currentTurn = NTURNS;
    final GOval[] lives = new GOval[NTURNS];

    final Brick[][] bricks = new Brick[NBRICK_ROWS][NBRICKS_PER_ROW];
    Brick brick;

    final GLabel scoreInfo = new GLabel("Score: " + score);
    final GLabel countInfo = new GLabel("Left Bricks: " + countBricks);

    public void run() {
        setupGame();
    }

    private void setupGame() {
        createBoard();
        createPaddle();
        createBall();
        waitForClick();
        startGame();
    }

    private void startGame() {
        ball.inGame = true;
        while (canPlay()) {
            ball.moved();
            checkObject();
            pause(10);
//            println("Brick left " + countBricks);
        }
//        println("Score:" + score);
    }

    private void checkObject() {
        GObject object = getObject();

        if (object == board.leftLine || object == board.rightLine || object == board.topLine
                || object == board.scoreBlock || object == board.bricksBlock || object == board.liveBlock) {
            ball.calcSpeedBall();
            println("board");
            println(object);
        } else if (object == board.bottomLine) {
//            ball.inGame = false;
            currentTurn--;
            drawLive();
            ball.setLocation(ballX, ballY);
            waitForClick();
            println("fail");
        } else if (object == paddle) {
            ball.vY = -ball.vY;
            println("paddle");
        } else if (object != null) {
            println(object);
            if (((Brick) object).isBrick) {
                println("Brick");
                brick = bricks[((Brick) object).row][((Brick) object).column];
                destroyBrick(brick);
                ball.vY = -ball.vY;

            } else {
                ball.inGame = false;
                println(object);
            }
        }
    }

    private void destroyBrick(@NotNull Brick brick) {
        score += brick.getScore();
        scoreInfo.setLabel("Score: " + score);
        countBricks--;
        countInfo.setLabel("Left Bricks: " + countBricks);
        remove(brick);
    }

    private @Nullable GObject getObject() {
        if (getElementAt(ball.getX(), ball.getY()) != null) {
            return (getElementAt(ball.getX(), ball.getY()));
        } else if (getElementAt((ball.getX() + BALL_RADIUS * 2), ball.getY()) != null) {
            return (getElementAt(ball.getX() + BALL_RADIUS * 2, ball.getY()));
        } else if (getElementAt(ball.getX(), ball.getY() + BALL_RADIUS * 2) != null) {
            return (getElementAt(ball.getX(), ball.getY() + BALL_RADIUS * 2));
        } else if (getElementAt(ball.getX() + BALL_RADIUS * 2, ball.getY() + BALL_RADIUS * 2) != null) {
            return (getElementAt(ball.getX() + BALL_RADIUS * 2, ball.getY() + BALL_RADIUS * 2));
        } else {
            return null;
        }
    }

    private boolean canPlay() {
        return ball.inGame && countBricks > 0 && currentTurn > 0;
    }

    private void createBoard() {
        board = new Board(getWidth(), getHeight());
        add(board.scoreBlock);
        add(board.bricksBlock);
        add(board.liveBlock);
        add(board.leftLine);
        add(board.topLine);
        add(board.rightLine);
        add(board.bottomLine);

        buildBricks();

        scoreInfo.setFont("SansSerif-14");
        add(scoreInfo, board.scoreBlock.getX() + 5, (board.scoreBlock.getHeight() - scoreInfo.getHeight() / 2));

        countInfo.setFont("SansSerif-14");
        add(countInfo, board.bricksBlock.getX() + 5, (board.bricksBlock.getHeight() - countInfo.getHeight() / 2));

        drawLive();
    }

    private void drawLive() {
        for (int i = 0; i < NTURNS; i++) {
            lives[i] = new GOval(board.liveBlock.getX() + 15 + board.liveBlock.getWidth() / 6 * i,
                    board.liveBlock.getHeight() / 3, 10, 10);

            Color color = i < currentTurn ? Color.green : Color.red;
            lives[i].setColor(color);
            lives[i].setFilled(true);
            add(lives[i]);
        }
    }

    private void buildBricks() {
        Color color;
        double x, y;
        for (int row = 0; row < NBRICK_ROWS; row++) {
            for (int n = 0; n < NBRICKS_PER_ROW; n++) {
                x = board.left + BRICK_WIDTH * n + BRICK_SEP * (n + 1);
                y = BRICK_Y_OFFSET + row * BRICK_HEIGHT + row * BRICK_SEP;
                color = switch (row / 2) {
                    case 0 -> Color.red;
                    case 1 -> Color.orange;
                    case 2 -> Color.yellow;
                    case 3 -> Color.green;
                    default -> Color.cyan;
                };
                bricks[row][n] = new Brick(x, y, BRICK_WIDTH, BRICK_HEIGHT, color, row, n);
                add(bricks[row][n]);
            }
        }
    }

    private void createPaddle() {
        double x = (getWidth() - PADDLE_WIDTH) / 2.0;
        paddleYCoord = getHeight() - PADDLE_HEIGHT - PADDLE_Y_OFFSET;
        paddle = new Paddle(x, paddleYCoord, PADDLE_WIDTH, PADDLE_HEIGHT, Color.BLACK, board);
        add(paddle);
        addMouseListeners();
    }

    private void createBall() {
        ballX = (getWidth() - BALL_RADIUS) / 2.0;
        ballY = (getHeight() - BALL_RADIUS) / 2.0;
        ball = new Ball(ballX, ballY, BALL_RADIUS * 2, Color.BLACK, board, paddle);
        add(ball);
    }

    public void mouseMoved(@NotNull MouseEvent e) {
        paddle.setPosition(e.getX());
    }
}
