/*
 *  File: Assignment3part5.java
 *_____________________________
 * Solves task "St. Petersburg game" :
 * This is a hypothetical game for a casino with a simple ideology.
 * Two people are playing: a lucky man and a sweaty one.
 * The game ends when the first one earns $ 20 or more.
 * Sweaty puts $ 1 on the table, and the lucky one starts tossing a coin.
 * If the eagle - then sweaty adds to the amount on the table exactly the same amount.
 * If the tail - everything on the table, goes to the lucky one.
 * If the lucky winner is less than $ 20, the game is repeated.
 *
 * Author: Chernov Sergey 10/13/20
 */

package com.shpp.p2p.cs.schernov.assignmet3;

import com.shpp.cs.a.console.TextProgram;

import java.util.Random;

public class Assignment3part5 extends TextProgram {
    private static final int SUM_FOR_WIN = 20;
    private static final int START_SUM = 1;

    int counterAttempt = 0;
    int totalWin = 0;

    /**
     * Specifies the program entry point
     */
    public void run() {
        runGame(START_SUM);
    }

    /**
     * The method runGame
     * recursive method play in game
     * when total win less SUM_FOR_WIN
     *
     * @param sum int current sum in to the table
     */
    private void runGame(int sum) {
        if (totalWin >= SUM_FOR_WIN) {
            println("It took " + counterAttempt + " games to earn $20");
        } else {
            int newSum;
            if (isEagleCoin()) {
                //sweaty put double coins
                newSum = sum * 2;
            } else {
                //lucky give table sum counter, total wins and start game from START_SUM
                totalWin += sum;
                counterAttempt++;
                newSum = START_SUM;
                println("This game, you earned $" + sum);
                println("Your total is $" + totalWin);
            }
            runGame(newSum);
        }
    }

    /**
     * The method isEagleCoin
     * random generator Boolean value
     * True - coin is eagle
     * False - coin is tail
     *
     * @return Boolean
     */
    private boolean isEagleCoin() {
        Random random = new Random();
        return random.nextBoolean();
    }


}
