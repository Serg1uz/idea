/*
 *  File: Messages.java
 *_____________________________
 * Solves task "" :
 *
 *
 * A simple algorithm for solving the  task:
 *
 * Author: Chernov Sergey 10/21/20
 */

package com.shpp.p2p.cs.schernov.assignmet4;

import acm.graphics.GLabel;
import org.jetbrains.annotations.NotNull;

public class Messages {

    public @NotNull GLabel score(int score) {
        GLabel label = new GLabel("Score: " + score);
        label.setFont("SansSerif-14");
        return label;
    }
}
