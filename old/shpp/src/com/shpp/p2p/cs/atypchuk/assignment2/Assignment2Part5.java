package com.shpp.p2p.cs.atypchuk.assignment2;

import acm.graphics.GRect;
import com.shpp.cs.a.graphics.WindowProgram;

import java.awt.*;

/**
 * this class create optical illusion with square
 * method used by:
 * createSquare();
 * createIllusionSquare();
 */
public class Assignment2Part5 extends WindowProgram {

    public static final int APPLICATION_WIDTH = 500;
    public static final int APPLICATION_HEIGHT =600;

    /* The number of rows and columns in the grid, respectively. */
    private static final int NUM_ROWS = 6;
    private static final int NUM_COLS = 6;

    /* The width and height of each box. */
    private static final double BOX_SIZE = 40;

    /* The horizontal and vertical spacing between the boxes. */
    private static final double BOX_SPACING = 10;

    // half the height and width of the illusion
    private static final double HALF_WIDTH_ILLUSION = (((BOX_SPACING-1)+BOX_SIZE)*NUM_COLS)/2;
    private static final double HALF_HEIGHT_ILLUSION = (((BOX_SPACING-1)+BOX_SIZE)*NUM_ROWS)/2;

    @Override
    public void run() {


        createIllusionSquare();
    }

    /**
     * this method creates a square by coordinates and side height
     *
     * @param x coordinate values for X
     * @param y coordinate values for Y
     */
    private void createSquare(double x, double y) {
        GRect square = new GRect(x, y, BOX_SIZE, BOX_SIZE);
        square.setFilled(true);
        square.setColor(Color.BLACK);
        add(square);
    }

    /**
     *  method for creating the illusion of squares
     */
    private void createIllusionSquare(){
        // start point
        double xO = getWidth()/2- HALF_WIDTH_ILLUSION;
        double yO = getHeight()/2- HALF_HEIGHT_ILLUSION;

        // create matrix
        for (int i = 1; i <= NUM_ROWS; i++) {
            for (int j = 1; j <= NUM_COLS; j++) {
                createSquare(xO, yO);
                xO += BOX_SPACING;//spacing with square
                xO+=BOX_SIZE; // coordinate new square
            }
            yO += BOX_SPACING; //create a new line spacing
            xO = getWidth()/2- HALF_WIDTH_ILLUSION; //start coordinate X
            yO +=BOX_SIZE; //start coordinate Y
        }
    }
}
