package com.shpp.p2p.cs.atypchuk.assignment2;

import acm.graphics.GLabel;
import acm.graphics.GRect;
import com.shpp.cs.a.graphics.WindowProgram;

import java.awt.*;

/**
 * this class draws a German flag
 * method used by:
 * createFlag();
 * createText();
 */
public class Assignment2Part4 extends WindowProgram {

    //width and height flag
    public static final int FLAG_WIDTH = 500;
    public static final int FLAG_HEIGHT = 400;

    //variables calculate the width in the vertical strip or the height in the vertical strip
    public static final int VERTICAL_STRIP = FLAG_WIDTH / 3;
    public static final int HORIZONTAL_STRIP = FLAG_HEIGHT / 3;

    public static final int APPLICATION_WIDTH = FLAG_WIDTH + FLAG_WIDTH / 4;
    public static final int APPLICATION_HEIGHT = FLAG_HEIGHT + FLAG_HEIGHT / 2;

    // colors which are not in standard delivery
    private static final Color COLOR_ONE = new Color(0, 0, 0);
    private static final Color COLOR_TWO = new Color(255, 255, 0);
    private static final Color COLOR_THREE = new Color(255, 0, 0);

    // if direction = true the direction to by horizontal else vertical
    private static final boolean DIRECTION_FLAG = true;

    //set text name of the country flag
    private static final String NAME_COUNTRY_FLAG = "Belgium";

    @Override
    public void run() {
        createFlag(FLAG_HEIGHT, FLAG_WIDTH);
        createText("Flag of " + NAME_COUNTRY_FLAG);
    }

    // the method that creates the flag
    private void createFlag(int height, int width) {
        int xCenter = getWidth() / 2;
        int yCenter = getHeight() / 2;

        //change direction flag
        if (DIRECTION_FLAG == true) {
            createFlagStrip(xCenter - VERTICAL_STRIP / 2 - VERTICAL_STRIP, yCenter - height / 2, VERTICAL_STRIP, height, COLOR_ONE);
            createFlagStrip(xCenter - VERTICAL_STRIP / 2, yCenter - height / 2, VERTICAL_STRIP, height, COLOR_TWO);
            createFlagStrip(xCenter + VERTICAL_STRIP / 2, yCenter - height / 2, VERTICAL_STRIP, height, COLOR_THREE);
        } else {
            createFlagStrip(xCenter - width / 2, yCenter - HORIZONTAL_STRIP / 2 - HORIZONTAL_STRIP, width, HORIZONTAL_STRIP, COLOR_ONE);
            createFlagStrip(xCenter - width / 2, yCenter - HORIZONTAL_STRIP / 2, width, HORIZONTAL_STRIP, COLOR_TWO);
            createFlagStrip(xCenter - width / 2, yCenter + HORIZONTAL_STRIP / 2, width, HORIZONTAL_STRIP, COLOR_THREE);
        }

    }

    /**
     * create flag strip
     *
     * @param x coordinate values for X
     * @param y coordinate values for Y
     * @param width  rectangle width
     * @param height rectangle height
     * @param color strip color
     */
    private void createFlagStrip(int x, int y, int height, int width, Color color) {
        GRect regtan = new GRect(x, y, height, width);
        regtan.setFilled(true);
        regtan.setColor(color);
        add(regtan);
    }

    /**
     * create label
     *
     * @param text the name of the country is transmitted here
     */
    private void createText(String text) {
        GLabel label = new GLabel(text);
        label.setFont("Comic Sans-18");
        label.setColor(Color.BLACK);
        double x = (getWidth() - label.getWidth());
        double y = (getHeight() - label.getAscent() / 2);
        add(label, x, y);
    }
}
