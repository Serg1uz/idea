package com.shpp.p2p.cs.atypchuk.assignment2;

import com.shpp.cs.a.console.TextProgram;

/**
 * This class solves the quadratic equation.
 * Methods used by:
 * findDiscriminant();
 * findRoots();
 */
public class Assignment2Part1 extends TextProgram {
    public void run() {

        double a, b, c;

        print("Please enter a: ");
        a = readDouble();
        print("Please enter b: ");
        b = readDouble();
        print("Please enter c: ");
        c = readDouble();

        //For a = 0, the quadratic equation will have no solution
        if (a == 0) {
            print("in the quadratic equation a != 0");
            return;
        }
        findRoots(a, b, c);
    }

    /**
     * this method searches for the roots x1 and x2
     *
     * @param a constant known value а
     * @param b constant known value b
     * @param c constant known value c
     */
    private void findRoots(double a, double b, double c) {

        double D = findDiscriminant(a, b, c);

        if (D < 0) {
            print("There are no real roots ");
        } else if (D == 0) {
            double x = -b / 2 * a;
            print("There is one root:" + x);
        } else {
            double x1 = (-b + D) / (2 * a);
            double x2 = (-b - D) / (2 * a);
            print("There is one root: " + x1 + " and " + x2);
        }
    }

    /**
     * this method searches for the discriminant
     *
     * @param a constant known value а
     * @param b constant known value b
     * @param c constant known value c
     * @return returns the value of the discriminant
     */
    private double findDiscriminant(double a, double b, double c) {
        return Math.sqrt((b * b) - 4 * a * c);
    }
}
