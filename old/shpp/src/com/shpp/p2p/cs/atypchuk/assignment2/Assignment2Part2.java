package com.shpp.p2p.cs.atypchuk.assignment2;

import acm.graphics.GOval;
import acm.graphics.GRect;
import com.shpp.cs.a.graphics.WindowProgram;

import java.awt.*;

/**
 * This class creates an optical illusion
 * Methods used by:
 * createCircle();
 * createSquare();
 */
public class Assignment2Part2 extends WindowProgram {

    public static final int APPLICATION_WIDTH = 300;
    public static final int APPLICATION_HEIGHT = 500;

    //diameter of the circle
    public static final int DIAMETER_CIRCLE = 100;

    public void run() {
        //width and height of windows
        int height = getHeight();
        int width = getWidth();

        createCircle(height, width);
        createCircle(height, DIAMETER_CIRCLE);
        createCircle(DIAMETER_CIRCLE, width);
        createCircle(DIAMETER_CIRCLE, DIAMETER_CIRCLE);

        createReg(DIAMETER_CIRCLE/2, DIAMETER_CIRCLE/2, width-DIAMETER_CIRCLE, height-DIAMETER_CIRCLE);
    }

    /**
     * this method creates a circle in coordinates and radius
     *
     * @param height coordinate values for X
     * @param width coordinate values for Y
     */
    private void createCircle(int height, int width) {
        //determine the initial coordinates
        double x = getWidth() - width;
        double y = getHeight() - height;

        GOval circle = new GOval(x, y, DIAMETER_CIRCLE, DIAMETER_CIRCLE);
        circle.setFilled(true);
        circle.setColor(Color.blue);
        add(circle);
    }

    /**
     * this method creates a square by coordinates and side height
     *
     * @param x coordinate values for X
     * @param y coordinate values for Y
     * @param width length sides of the rectangle
     * @param height length sides of the rectangle
     */
    private void createReg(int x, int y, int width, int height) {
        GRect regtan = new GRect(x, y, width, height);
        regtan.setFilled(true);
        regtan.setColor(Color.white);
        add(regtan);
    }

}
