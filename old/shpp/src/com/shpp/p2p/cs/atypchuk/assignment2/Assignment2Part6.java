package com.shpp.p2p.cs.atypchuk.assignment2;

import acm.graphics.GOval;
import com.shpp.cs.a.graphics.WindowProgram;

import java.awt.*;

/**
 * this class draw a caterpillar
 * method used by:
 * createCaterpillar();
 * createASegment();
 */
public class Assignment2Part6 extends WindowProgram {

    /* The number of segment in the grid, respectively. */
    private static final int NUM_SEG = 10;

    /* The width and height of each box. */
    private static final double OVAL_SIZE = 100;

    /* The horizontal and vertical spacing between the oval. */
    private static final double OVAL_SPACING = 60;

    public static final int APPLICATION_WIDTH = (int) ((OVAL_SIZE - OVAL_SPACING / 2) * NUM_SEG);
    public static final int APPLICATION_HEIGHT = (int) (OVAL_SIZE + OVAL_SPACING);

    public void run() {
        createCaterpillar();
    }

    /**
     * this method creates a segment(oval)
     *
     * @param x coordinate values for X
     * @param y coordinate values for Y
     */
    private void createASegment(double x, double y) {
        GOval oval = new GOval(x, y, OVAL_SIZE, OVAL_SIZE);
        GOval border = new GOval(x, y, OVAL_SIZE, OVAL_SIZE);
        oval.setFilled(true);
        oval.setColor(Color.GREEN);
        border.setColor(Color.RED);
        add(oval);
        add(border);
    }

    /**
     * this method creates a caterpillar
     */
    private void createCaterpillar() {
        // start point
        double xO = 0;
        double yO = OVAL_SPACING / 2;

        // create caterpillar
        for (int i = 1; i <= NUM_SEG; i++) {
            createASegment(xO, yO);

            // looking for even and odd numbers to place the segments of the caterpillar
            if (i % 2 == 0) {
                yO += OVAL_SPACING / 2;
                xO += OVAL_SPACING;
                continue;
            }
            yO -= OVAL_SPACING / 2;
            xO += OVAL_SPACING;
        }
    }
}
