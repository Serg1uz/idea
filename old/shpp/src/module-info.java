
/*
 *  File: module-info.java
 *_____________________________
 * Solves task "" :
 *
 *
 * A simple algorithm for solving the  task:
 *
 * Author: Chernov Sergey 10/21/20
 */

//package;
module shpp {
    requires java.datatransfer;
    requires java.desktop;
    requires org.jetbrains.annotations;
    requires shpp.cs.java.lib;
}