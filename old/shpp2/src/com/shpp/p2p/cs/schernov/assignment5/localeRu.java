/*
 * File: localeRu.java
 *_____________________________
 *
 * Simple class when description some rules
 * for calculating syllables count in Ru word
 */
package com.shpp.p2p.cs.schernov.assignment5;

public class localeRu {
    //global variables array volves letter
    private final char[] volves = new char[]{'а', 'у', 'о', 'ы', 'и', 'э', 'я', 'ю', 'ё', 'е'};

    //global variables word for calculating
    String word;
    //global variables original word for calculating
    String original_word;

    /**
     * Constructor for class
     * @param word String word for calculating
     */
    public localeRu(String word) {
        this.word = word;
        this.original_word = word;
    }

    /**
     * The method count
     * Calculating all syllables in word
     * @return int count of syllables
     */
    public int count() {
        int result = 0;
        word = word.toLowerCase();

        if (word.endsWith("e")) {
            word = word.substring(0, word.length() - 1);
        }

        result += simpleCount();


        return result == 0 ? 1 : result;
    }

    /**
     * The method simpleCount
     * Calculating syllables count use basic rule
     * @return int count of basic rule in word
     */
    private int simpleCount() {
        int result = 0;

        // count once volve
        for (int i = 0; i < word.length(); i++) {
            char ch = word.charAt(i);
            result += checkChar(ch, volves) ? 1 : 0;
        }
        return result;
    }

    /**
     * The merhod checkChar
     * checked some char in array of char
     * @param ch char some char what checked
     * @param chars chars[] array of char when checked
     * @return boolean true if char in array else false
     */
    private boolean checkChar(char ch, char[] chars) {
        for (char c : chars) {
            if (c == ch) {
                return true;
            }
        }
        return false;
    }
}
