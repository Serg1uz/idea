/*
 * File: SyllableCounting.java
 *_____________________________
 *
 * This file solved task "Calculate syllable count"
 * We can detect two language EN and RU
 * And use simple rules calculated count of syllable in word
 */

package com.shpp.p2p.cs.schernov.assignment5;

import com.shpp.cs.a.console.TextProgram;

public class SyllableCounting extends TextProgram {
    //global variable asked word
    String word;

    /**
     * Main method in programs
     */
    @SuppressWarnings("InfiniteLoopStatement")
    public void run() {
        simpleTest();
        while (true) {
            this.word = readLine("Enter a single word: ");
            println("  Syllable count: " + syllablesIn(this.word));
        }

    }

    /**
     * The method simpleTest
     * Resolved small test for calculating count of syllables
     * in array of string testStr need said first element word
     * and second element count of syllables from dictionary
     * In the test equals count of syllables from dictionary
     * and calculating by our method
     */
    private void simpleTest() {
        String[][] testStr = new String[][]{{"Unity", "3"},
                {"Unite", "2"},
                {"Мама", "2"},
                {"Growth", "1"},
                {"Me", "1"},
                {"Nimble", "2"},
                {"Beautiful", "3"},
                {"Manatee", "3"},
                {"Meagrely", "2"},
                {"Administratively", "6"},
        };
        for (String[] strings : testStr) {
            int count = syllablesIn(strings[0]);
            if (count == -1) {
                println("Skip " + strings[0] + " Understand language ");
                continue;
            }

            int testCount = Integer.parseInt(strings[1]);
            if (count != testCount) {
                println("Test Broken: " + strings[0] + " " + strings[1] + " show " + count);
                break;
            }
            println("Test completed: " + strings[0] + " " + strings[1] + " show " + count);

        }
    }

    /**
     * The method syllablesIn
     * from detecting language use need rule class for calculating
     * syllables count
     *
     * @return An estimate of the number of syllables in that word
     * or -1 if language not detected
     */
    private int syllablesIn(String word) {
        switch (detectLanguage(word)) {
            case "EN":
                localeEng le = new localeEng(word);
                return le.count();
            case "RU":
                localeRu lr = new localeRu(word);
                return lr.count();
            case "Error":
                return -1;
        }
        return -1;
    }

    /**
     * The method detectLanguage
     * from the check ASCII Code detect language
     *
     * @param str String word for detect language
     * @return String detecting language En/Ru
     * or Error if language can't detect
     */
    private String detectLanguage(String str) {
        str = str.toLowerCase();
        //EN
        boolean check = true;
        for (int i = 0; i < str.length(); i++) {
            int code = str.charAt(i);
            if (code < 97 || code > 122) {
                check = false;
                break;
            }
        }
        if (check) {
            return "EN";
        }
        //RU
        check = true;
        for (int i = 0; i < str.length(); i++) {
            int code = str.charAt(i);
            if (code < 1072 || code > 1105) {
                check = false;
                break;
            }
        }
        if (check) {
            return "RU";
        }
        return "Error";
    }

}
