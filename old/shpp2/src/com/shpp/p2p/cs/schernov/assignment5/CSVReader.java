/*
 * File: CSVReader.java
 *_____________________________
 *
 * Simple class for parsing CSV file
 */
package com.shpp.p2p.cs.schernov.assignment5;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class CSVReader {
    // global variables file name
    private String filename;
    // global variable used headers into csv file or not
    private boolean useHeaders = false;
    // global variables headers from  csv file
    private List<String> headers;
    // global variables data from csv file
    private final HashMap<String, List<String>> data = new HashMap<>();

    // global variable separator for column
    private char separator = ',';
    // global variable mark character for quotes
    private char quotes = '"';
    // global variable csv file has error or not
    private boolean hasError = false;
    // global variable error messages
    private String errorMessage = "No Error";

    /**
     * Constructor for class
     * set file name only, other parameters defaults
     * @param filename String csv file name
     */
    public CSVReader(String filename) {
        this(filename, false, ',', '"');
    }

    /**
     * Constructor for class
     * set file name  and used headers into csv file only, other parameters defaults
     * @param filename String csv file name
     * @param useHeaders boolean csv file used headers or not
     */
    public CSVReader(String filename, boolean useHeaders) {
        this(filename, useHeaders, ',', '"');
    }

    /**
     * Constructor for class (Main)
     * Set all params for csv file
     * @param filename String csv file name
     * @param useHeaders boolean csv file used headers or not
     * @param separator char separator delimiter
     * @param quotes char quotes delimiter
     */
    public CSVReader(String filename, boolean useHeaders, char separator, char quotes) {
        this.setFilename(filename);
        this.setUseHeaders(useHeaders);
        this.setSeparator(separator);
        this.setQuotes(quotes);
        readFile();
    }

    /**
     * The method readFile
     * Read csv file or raise some error
     */
    private void readFile() {
        clearError();
        try {
            BufferedReader br = new BufferedReader(new FileReader(filename));
            int line = 0;
            while (true) {
                String row = br.readLine();
                if (row == null) break;
                parseRow(row, line);
                line++;
            }
        } catch (IOException e) {
            this.hasError = true;
            this.errorMessage = "Error in Read File: " + e.getMessage();
        }
    }

    /**
     * The method clearError
     * set default error parameters
     */
    private void clearError() {
        hasError = false;
        errorMessage = "No Error";
    }

    /**
     * The method  parseRow
     * parsing row from csv file
     * @param row String original text in row
     * @param line int current number line from csv file
     */
    private void parseRow(String row, int line) {
        clearError();
        List<String> result = new ArrayList<>();
        StringBuffer currenValue = new StringBuffer();
        boolean isQuotes = false;

        char[] chars = row.toCharArray();
        for (char aChar : chars) {
            if (hasError) break;
            if (isQuotes) {
                currenValue.append(aChar);
                if (aChar == '"') isQuotes = false;
            } else {
                if (aChar == '"') {
                    currenValue.append(aChar);
                    isQuotes = true;

                } else if (aChar == ',') {
                    result.add(currenValue.toString());
                    currenValue = new StringBuffer();
                } else {
                    currenValue.append(aChar);
                }
            }
        }
        if (!hasError) {
            result.add(currenValue.toString());
            if (line == 0) setHeaders(result);
            else setData(result);
        }
    }

    /**
     * Getter for private variable file name
     * @return String file name
     */
    public String getFilename() {
        return filename;
    }

    /**
     * Setter or private variable file name
     * @param filename String file name
     */
    public void setFilename(String filename) {
        this.filename = filename;
    }

    /**
     * Getter for private variable use header
     * @return String file name
     */
    public boolean getUseHeaders() {
        return useHeaders;
    }
    
    /**
     * Setter or private variable use headers
     * @param useHeaders boolean use headers or not
     */
    public void setUseHeaders(boolean useHeaders) {
        this.useHeaders = useHeaders;
    }
    
    /**
     * Getter for private variable separator to value
     * @return char separator
     */
    public char getSeparator() {
        return separator;
    }

    /**
     * Setter or private variable separator to value
     * @param separator char separator to value
     */
    public void setSeparator(char separator) {
        this.separator = separator;
    }

    /**
     * Getter for private variable quotes for values
     * @return char quotes
     */
    public char getQuotes() {
        return quotes;
    }
    
    /**
     * Setter or private variable quotes to value
     * @param quotes char quotes to value
     */
    public void setQuotes(char quotes) {
        this.quotes = quotes;
    }
    
    /**
     * Getter for private variable has error in csv file
     * @return boolean true if csv file has error else false
     */
    public boolean getHasError() {
        return hasError;
    }
    
    /**
     * Getter for private variable has error message in csv file
     * @return String error message
     */
    public String getErrorMessage() {
        return errorMessage;
    }
    
    /**
     * Getter for private variable headers for csv file
     * @return List<String> array of headers
     */
    public List<String> getHeaders() {
        return headers;
    }
    
    /**
    * The method set headers by name if used headers or number
    */
    private void setHeaders(List<String> result) {
        if (useHeaders) this.headers = result;
        else {
            this.headers = new ArrayList<>(result.size());
            for (int i = 0; i < result.size(); i++) {
                this.headers.add(String.valueOf(i));
            }
            setData(result);
        }
    }
    
    /**
    * The method return all data from csv file
    */
    public HashMap<String, List<String>> getData() {
        return data;
    }
    
    /**
    * The method set data from csv file
    */
    private void setData(List<String> result) {
        if (result.size() != headers.size()) {
            hasError = true;
            errorMessage = "Error in line size: To many separators";
        } else {
            for (int i = 0; i < result.size(); i++) {
                String key = headers.get(i);
                List<String> value = Collections.singletonList(result.get(i));
                List<String> list = data.get(key);
                if (list == null) {
                    data.put(key, value);
                } else {
                    ArrayList<String> mergedValue = new ArrayList<>(value);
                    mergedValue.addAll(list);
                    data.put(key, mergedValue);
                }
            }
        }
    }
    
    /**
    * The method return all data from csv file by key name
    */
    public List<String> getDataByKey(String key) {
        return data.get(key);
    }

    /**
    * The method return range data from csv file by key name from id to id
    */
    public List<String> getDataByKey(String key, int idFrom, int idTo) {
        return data.get(key).subList(idFrom, idTo);
    }

    /**
    * The method return all data from csv file by column number
    */
    public List<String> getDataByKey(int idColumn) {
        return getDataByKey(headers.get(idColumn));
    }
    
    /**
    * The method return range data from csv file by column number from id to id
    */
    public List<String> getDataByKey(int idColumn, int idFrom, int idTo) {
        return getDataByKey(headers.get(idColumn), idFrom, idTo);
    }
    
    /**
    * The method return full data from csv file by id
    */
    public HashMap<String, List<String>> getDataById(int id) {
        HashMap<String, List<String>> result = new HashMap<>();
        for (String key : headers) {
            List<String> value = data.get(key);
            result.put(key, Collections.singletonList(value.get(id)));
        }
        return result;
    }
    
    /**
    * The method return full data from csv file by range id
    */
    public HashMap<String, List<String>> getDataByRangeId(int idFrom, int IdTo) {
        HashMap<String, List<String>> result = new HashMap<>();
        for (String key : headers) {
            List<String> value = data.get(key).subList(idFrom, IdTo);
            result.put(key, value);
        }
        return result;
    }

}
