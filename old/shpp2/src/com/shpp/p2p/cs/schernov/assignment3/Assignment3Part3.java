/*
 *  File: Assignment3part3.java
 *_____________________________
 * Solves task "Raise To Power" :
 * calculate the value of the first parameter reduced to the power of the parameter
 *
 * A simple algorithm for solving the  task:
 * 1. get input data
 * 2. calculate result
 *
 * Author: Chernov Sergey 10/13/20
 */

package com.shpp.p2p.cs.schernov.assignment3;

import com.shpp.cs.a.console.TextProgram;

public class Assignment3Part3 extends TextProgram {
    /**
     * Specifies the program entry point
     */
    public void run() {
        //get input data
        double base = readDouble("Input Base:");
        int exponent = readInt("Input Exponent");
        //show result
        println("Result: " + raiseToPower(base, exponent));
    }

    /**
     * The method raiseToPower
     * base parameter reduced to the power exponent parameter
     * using simple line-algorithm
     * default result equal 1.0 - for than exponent = 0
     * if exponent more 0 using loops
     * else using loops and result = 1/result(loops)
     *
     * @param base     double Base parameter
     * @param exponent int Exponent parameter
     * @return double Result base raise to power Exponent
     */
    private double raiseToPower(double base, int exponent) {
        double result = 1.0;
        int absExponent = exponent < 0 ? -exponent : exponent;
        for (int i = 0; i < absExponent; i++) {
            result *= base;
        }
        if (exponent < 0) {
            result = 1.0 / result;
        }
        return result;
    }
}
