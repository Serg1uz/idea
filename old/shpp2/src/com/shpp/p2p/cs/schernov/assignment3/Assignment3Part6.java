/*
 *  File: Assignment3part6.java
 *_____________________________
 * Solves task "Animation" :
 * 5 seconds animation
 * complete logo SHPP from many puzzles
 *
 * A simple algorithm for solving the  task:
 * 1. draw background
 * 2. generated puzzles
 * 3. show puzzles in  start position
 * 4. show animation when all puzzles not in finish position
 *
 * Author: Chernov Sergey 10/13/20
 */

package com.shpp.p2p.cs.schernov.assignment3;

import acm.graphics.GRect;
import com.shpp.cs.a.graphics.WindowProgram;

import java.awt.*;
import java.util.Random;

/**
 * Class Puzzle
 * This is helper class description object puzzle
 * All puzzle is square have:
 * start position, finish position, size, color
 * count of steps animation, delta coords
 */
class Puzzle {
    //main object
    GRect rect;
    //variables for main object
    double startX, startY;
    double finishX, finishY;
    double width, height;
    Color color;

    //variables for animation
    int steps;
    double deltaX, deltaY;

    /**
     * The method for init class Puzzle
     *
     * @param startX  double start X-Coordinate
     * @param startY  double start Y-Coordinate
     * @param finishX double finish X-Coordinate
     * @param finishY double finish Y-Coordinate
     * @param width   double size width
     * @param height  double size height
     * @param steps   int count of step animation
     * @param color   Color color puzzle
     */
    public Puzzle(double startX, double startY, double finishX, double finishY, double width, double height, int steps, Color color) {
        this.startX = startX;
        this.startY = startY;
        this.finishX = finishX;
        this.finishY = finishY;
        this.width = width;
        this.height = height;
        this.color = color;
        this.steps = steps;
        createRect();
        calculated();
    }

    /**
     * The method moved
     * one step animation
     * move main object by dX dY Coordinates
     */
    public void moved() {
        this.rect.move(this.deltaX, this.deltaY);
    }

    /**
     * The method createRect
     * create GRect
     * main object in this class
     */
    private void createRect() {
        this.rect = new GRect(startX, startY, width, height);
        this.rect.setColor(color);
        this.rect.setFilled(true);
    }

    /**
     * The method calculated
     * calculate delta XY-Coordinate for animation
     */
    private void calculated() {
        this.deltaX = (finishX - startX) / steps;
        this.deltaY = (finishY - startY) / steps;
    }
}

public class Assignment3Part6 extends WindowProgram {
    //Global const for size screen DONT CHANGE
    public static final int APPLICATION_WIDTH = 850;
    public static final int APPLICATION_HEIGHT = 400;
    //Global const for count of steps animation
    public static final int STEPS = 70;

    //Global variables for start position
    double firstX, firstY;
    //Array of puzzles
    Puzzle[] puzzles = new Puzzle[1000];
    //Global variables for count of Puzzle
    int index = 0;

    /**
     * Specifies the program entry point
     */
    public void run() {
        drawBackGround();
        getCentered();
        generateShBoxes();
        generatePlus(firstX + 10 * 38, firstY + 10 * 10);
        generatePlus(firstX + 10 * 58, firstY + 10 * 10);
        drawPicture();
        showMotion();
    }

    /**
     * The method drawBackGround
     * draw rectangle to all screen
     * with black filled color
     */
    private void drawBackGround() {
        drawRectangle(0, 0, getWidth(), getHeight(), Color.black);
    }

    /**
     * The method getCentered
     * calculate center of picture
     * and set global variables for start position
     */
    private void getCentered() {
        this.firstX = (getWidth() - 700) / 2.0;
        this.firstY = (getHeight() - 200) / 2.0;
    }

    /**
     * The method generateShBoxes
     * Generated puzzles for part of logo - "Ш"
     */
    private void generateShBoxes() {
        double x, y;
        Color color = Color.blue;
        //first I
        x = firstX;
        y = firstY;
        generateBoxes(x, y, 21, 6, color);
        //first _
        x = firstX + 10 * 6;
        y = firstY + 10 * 16;
        generateBoxes(x, y, 5, 8, color);
        //second I
        x = firstX + 10 * 14;
        y = firstY;
        generateBoxes(x, y, 21, 6, color);
        //second _
        x = firstX + 10 * 20;
        y = firstY + 10 * 16;
        generateBoxes(x, y, 5, 8, color);
        //second I
        x = firstX + 10 * 28;
        y = firstY;
        generateBoxes(x, y, 21, 6, color);
    }

    /**
     * The method generatePlus
     * Generated puzzles for part of logo - "+"
     *
     * @param x double X-Coordinate start position
     * @param y double X-Coordinate start position
     */
    private void generatePlus(double x, double y) {
        double x1, y1;
        Color color = Color.green;
        //first -

        x1 = x;
        y1 = y;
        generateBoxes(x1, y1, 4, 6, color);


        // I
        x1 = x1 + 6 * 10;
        y1 = y1 - 5 * 10;
        generateBoxes(x1, y1, 14, 5, color);

        //second -
        x1 = x1 + 5 * 10;
        y1 = y;
        generateBoxes(x1, y1, 4, 6, color);

    }

    /**
     * The method generateBoxes
     * generated puzzle in the part of logo
     *
     * @param x      double finish X-Coordinate
     * @param y      double finish Y-Coordinate
     * @param row    int count of row
     * @param column int count of column
     * @param color  Color color of puzzle
     */
    private void generateBoxes(double x, double y, int row, int column, Color color) {
        double x1, y1;
        x1 = x;
        y1 = y;
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < column; j++) {

                puzzles[index] = new Puzzle(randomCoordinate(getWidth()), randomCoordinate(getHeight()),
                        x1 + 10 * j, y1 + 10 * i, 10, 10, STEPS, color);
                index++;
            }
        }
    }

    /**
     * The method randomCoordinate
     *
     * @param maxValue double max value
     * @return double random number from range 10 .. Max Value
     */
    private double randomCoordinate(double maxValue) {
        Random r = new Random();
        return 10 + (maxValue - 10) * r.nextDouble();
    }

    /**
     * The method drawPicture
     * show in screen all puzzles
     * with start position all
     */
    private void drawPicture() {
        for (int i = 0; i < index; i++) {
            add(puzzles[i].rect);
        }
    }

    /**
     * The method showMotion
     * animated method
     * in loop by count of step moved all puzzles
     * to finish position
     */
    private void showMotion() {
        for (int step = 0; step < STEPS; step++) {
            for (int i = 0; i < index; i++) {
                puzzles[i].moved();
            }
            pause(STEPS);
        }
    }

    /**
     * The method drawRectangle
     * draw  rectangle
     *
     * @param x      double start X-Coordinate rectangle
     * @param y      double start Y-Coordinate rectangle
     * @param width  double size width
     * @param height double size height
     * @param color  Color color of rectangle
     */
    private void drawRectangle(double x, double y, double width, double height, Color color) {
        GRect rect = new GRect(x, y, width, height);
        rect.setFilled(true);
        rect.setColor(color);
        add(rect);
    }
}
