/*
 * File: Ball.java
 *_____________________________
 *
 * A class that physically describes the ball itself and its behavior
 * Extends of GOval object
 * used option:
 * delta XY-Coordinate, max speed, is moved, start XY-Coordinate, radius
 * and methods
 * invert delta XY-Coordinate, start move, move, set default options, set speed move and get points of ball
 */
package com.shpp.p2p.cs.schernov.assignment4;

import acm.graphics.GOval;
import acm.graphics.GPoint;

import java.awt.*;

public class Ball extends GOval {
    //speed coefficient
    private static final double MAX_SPEED = 3.0;
    /* Ball options */
    //delta XY-Coordinates for move ball
    double dX, dY;
    //flag for moved ball
    boolean isMoved;
    //start XY-Coordinate for ball
    double startX, startY;
    //radius ball
    double radius;
    //private
    /* end ball options */

    /**
     * init class
     *
     * @param x      start X-Coordinate (double)
     * @param y      start Y-Coordinate (double)
     * @param radius radius of ball (double)
     **/
    public Ball(double x, double y, double radius) {
        super(x, y, radius * 2, radius * 2);
        this.setColor(Color.BLACK);
        this.setFilled(true);
        // set default values
        setDefault(x, y, radius);
    }

    /**
     * method invert delta X-Coordinate
     * for change direction ball move
     **/
    public void invertDX() {
        dX = -dX;
    }

    /**
     * method invert delta Y-Coordinate
     * for change direction ball move
     **/
    public void invertDY() {
        dY = -dY;
    }


    /**
     * method set random delta XY-Coordinate
     * from range 1 to 3
     * and to delta X-Coordinate set +/-
     * <p>
     * set ball moved
     * run ball move animation
     **/
    public void startMove() {
        dY = Helper.getRandom(1.0, MAX_SPEED);
        dX = Helper.getRandom(1.0, MAX_SPEED);
        if (Helper.getRandom(0.5)) invertDX();
        isMoved = true;
        moved();
    }

    /**
     * The method move ball with
     * delta XY-Coordinate
     **/
    public void moved() {
        this.move(dX, dY);
    }

    /**
     * The method set default ball options
     *
     * @param x      start X-Coordinate (double)
     * @param y      start Y-Coordinate (double)
     * @param radius radius of ball (double)
     */
    private void setDefault(double x, double y, double radius) {
        dX = 0;
        dY = 0;
        isMoved = false;
        startX = x;
        startY = y;
        this.radius = radius;
    }

    /**
     * The method setSpeed
     * set speed ball for move by increase delta XY-Coordinates
     * in coefficient with limit by MAX_SPEED
     *
     * @param x double coefficient for  increase delta XY-Coordinates
     */
    public void setSpeed(double x) {
        dX = Math.abs(dX * x) >= MAX_SPEED ? MAX_SPEED * Math.abs(x) / x : Math.abs(dX) * x;
        dY = Math.abs(dY * x) >= MAX_SPEED ? MAX_SPEED : MAX_SPEED * x;
    }

    /**
     * The method getLeftMaxPoint
     * get max point for left of ball
     *
     * @return GPoint XY-Coordinate
     */
    public GPoint getLeftMaxPoint() {
        return new GPoint(this.getX(), this.getY() + radius);
    }

    /**
     * The method getTopMaxPoint
     * get max point for top of ball
     *
     * @return GPoint XY-Coordinate
     */
    public GPoint getTopMaxPoint() {
        return new GPoint(this.getX() + radius, this.getY());
    }

    /**
     * The method getRightMaxPoint
     * get max point for right of ball
     *
     * @return GPoint XY-Coordinate
     */
    public GPoint getRightMaxPoint() {
        return new GPoint(this.getX() + radius * 2, this.getY() + radius);
    }

    /**
     * The method getBottomMaxPoint
     * get max point for bottom of ball
     *
     * @return GPoint XY-Coordinate
     */
    public GPoint getBottomMaxPoint() {
        return new GPoint(this.getX() + radius, this.getY() + radius * 2);
    }

    /**
     * The method getAllMaxPoints
     * get all max point for  ball
     *
     * @return array GPoint XY-Coordinate
     */
    public GPoint[] getAllMaxPoints() {
        return new GPoint[]{getLeftMaxPoint(), getTopMaxPoint(), getRightMaxPoint(), getBottomMaxPoint()};
    }

}
