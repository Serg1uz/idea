/*
 *  File: Assignment3Part2.java
 *_____________________________
 * Solves task "Garden numbers" :
 * It is necessary to bring the input number to one using a special algorithm
 * Special algorithm:
 * if number odd then number = 3 * number + 1
 * if number even then number = number / 2
 * if number equal 1 then finish program
 *
 * A simple algorithm for solving the  task:
 * 1. get input number
 * 2. using recursive method bring input to one
 *
 * Author: Chernov Sergey 10/13/20
 */

package com.shpp.p2p.cs.schernov.assignment3;


import com.shpp.cs.a.console.TextProgram;

public class Assignment3Part2 extends TextProgram {


    /**
     * Specifies the program entry point
     */
    public void run() {
        int number = readInt("Enter a number:");
        if (number > 1) {
            showGardenNumbers(number);
        } else {
            //special case this algorithm work only for numbers more 1 print Error
            println("Error: Input number must be more 1 !");
        }
    }

    /**
     * The method showGardenNumbers
     * using recursive method
     * show current number, type number (odd/even), show next number
     * exit from recursion if current number equal 1
     *
     * @param number int Number for calc and show
     */
    private void showGardenNumbers(int number) {
        if (number == 1) {
            //exit from recursive
            println("The End!");
        } else {
            int newNumber;
            if (number % 2 == 0) {
                //number is even
                newNumber = number / 2;
                println(number + " is even so I take half: " + newNumber);
            } else {
                //number is odd
                newNumber = 3 * number + 1;
                println(number + " is odd so I make 3n + 1: " + newNumber);
            }
            showGardenNumbers(newNumber);
        }
    }
}
