/*
 * File: localeEng.java
 *_____________________________
 *
 * Simple class when description some rules
 * for calculating syllables count in EN word
 */
package com.shpp.p2p.cs.schernov.assignment5;

public class localeEng {
    //global variables array volves letter
    private final char[] volves = new char[]{'a', 'e', 'i', 'o', 'u', 'y'};
    //global variables array sonates letter
    private final char[] sonates = new char[]{'l'};
    //global variables array nosiy letter
    private final char[] noisyConsonats = new char[]{'b', 's', 't', 'p', 'd', 'h'};
    //global variables word for calculating
    String word;
    //global variables original word for calculating
    String original_word;

    /**
     * Constructor for class
     * @param word String word for calculating
     */
    public localeEng(String word) {
        this.word = word;
        this.original_word = word;
    }

    /**
     * The method count
     * Calculating all syllables in word
     * @return int count of syllables
     */
    public int count() {
        int result = 0;
        word = word.toLowerCase();

        if (word.endsWith("e")) {
            word = word.substring(0, word.length() - 1);
        }

        result += simpleCount();

        result += additionalCount();

        result -= exceptionCount();

        return result == 0 ? 1 : result;
    }

    /**
     * The method exceptionCount
     * Calculating some exception from rules
     * @return int count of exception from rules in word
     */
    private int exceptionCount() {
        int result = 0;
        word = original_word;
        //rule for -ely
        if (word.endsWith("ely")) {
            result++;
        }
        return result;
    }

    /**
     * The method additionalCount
     * Calculating some additional syllable from rules
     * @return int count of additional count from rules in word
     */
    private int additionalCount() {
        int result = 0;
        word = original_word;
        for (int i = 1; i < word.length() - 1; i++) {
            char ch = word.charAt(i);
            char prevCh = word.charAt(i - 1);
            char nextCh = word.charAt(i + 1);

            // rule for sonates
            if (checkChar(ch, sonates)) {
                if (checkChar(nextCh, volves) && checkChar(prevCh, noisyConsonats)) {
                    result++;
                }
            }
        }

        // rule for -re
        if (word.endsWith("re")) {
            char prevEnd = word.charAt(word.length() - 3);
            char ch = word.charAt(word.length() - 4);
            if (checkChar(ch, volves) && prevEnd == 'w') {
                result++;
            }
        }
        return result;
    }

    /**
     * The method simpleCount
     * Calculating syllables count use basic rule
     * @return int count of basic rule in word
     */
    @SuppressWarnings("UnnecessaryContinue")
    private int simpleCount() {
        int result = 0;

        // count once volve
        for (int i = 0; i < word.length(); i++) {
            char ch = word.charAt(i);
            if (checkChar(ch, volves)) {
                if (i + 1 < word.length()) {
                    char ch_next = word.charAt(i + 1);
                    if (checkChar(ch_next, volves)) {
                        continue;
                    } else {
                        result++;
                    }
                } else {
                    result++;
                }
            }
        }
        return result;
    }

    /**
     * The merhod checkChar
     * checked some char in array of char
     * @param ch char some char what checked
     * @param chars chars[] array of char when checked
     * @return boolean true if char in array else false
     */
    private boolean checkChar(char ch, char[] chars) {
        for (char c : chars) {
            if (c == ch) {
                return true;
            }
        }
        return false;
    }
}
