package com.shpp.p2p.cs.schernov.test;


import com.shpp.cs.a.console.TextProgram;

import java.awt.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Test extends TextProgram {
    private final static String EXCEPTIONS_PATH
            = "english-exceptions.txt",
            SUBSYL_PATH = "english-subsyls.txt",
            ADDSYL_PATH = "english-addsyls.txt";

    private Map<String, Integer> exceptions;

    private  Set<Pattern> subSyls, addSyls;

    private  Set<Character> vowels;

    String word;

    Stream<String> getRessourceLines(Class<?> clazz, String filepath) {
        try (final BufferedReader fileReader = new BufferedReader(
                new InputStreamReader(clazz.getResourceAsStream(filepath),
                        StandardCharsets.UTF_8)
        )) {
            // Collect the read lines before converting back to a Java stream
            // so that we can ensure that we close the InputStream and prevent leaks
            return fileReader.lines().collect(Collectors.toList()).stream();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void run() {
        exceptions = getRessourceLines(getClass(), EXCEPTIONS_PATH)
                .filter(line -> !line.isEmpty() && !line.startsWith("#"))
                .map(line -> line.split(" "))
                .peek(fields -> {
                    if (fields.length != 2) {
                        System.err.println("couldn't parse the exceptions "
                                + "file. Didn't find 2 fields in one of "
                                + "the lines.");
                    }
                })
                .collect(Collectors.toMap(
                        fields -> fields[1],
                        fields -> Integer.parseInt(fields[0])));
        addSyls = getRessourceLines(getClass(), ADDSYL_PATH)
                .filter(line -> !line.isEmpty() && !line.startsWith("#"))
                .map(Pattern::compile)
                .collect(Collectors.toSet());
        subSyls = getRessourceLines(getClass(), SUBSYL_PATH)
                .filter(line -> !line.isEmpty() && !line.startsWith("#"))
                .map(Pattern::compile)
                .collect(Collectors.toSet());
        vowels = new HashSet<>(Arrays.asList('a', 'e', 'i', 'o', 'u', 'y'));

        while (true) {
            this.word = readLine("Enter a single word: ");
            println("  Syllable count: " + count(word));
        }
    }

    public int count(String word) {
        if (word == null) {
            throw new NullPointerException("the word parameter was null.");
        } else if (word.length() == 0) {
            return 0;
        } else if (word.length() == 1) {
            return 1;
        }

        final String lowerCase = word.toLowerCase();

        if (exceptions.containsKey(lowerCase)) {
            return exceptions.get(lowerCase);
        }

        final String prunned;
        if (lowerCase.charAt(lowerCase.length() - 1) == 'e') {
            prunned = lowerCase.substring(0, lowerCase.length() - 1);
        } else {
            prunned = lowerCase;
        }

        int count = 0;
        boolean prevIsVowel = false;
        for (char c : prunned.toCharArray()) {
            final boolean isVowel = vowels.contains(c);
            if (isVowel && !prevIsVowel) {
                ++count;
            }
            prevIsVowel = isVowel;
        }
        count += addSyls.stream()
                .filter(pattern -> pattern.matcher(prunned).find())
                .count();
        count -= subSyls.stream()
                .filter(pattern -> pattern.matcher(prunned).find())
                .count();

        return count > 0 ? count : 1;
    }
}
