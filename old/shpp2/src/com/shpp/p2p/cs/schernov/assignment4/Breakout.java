/*
 * File: Breakout.java
 *_____________________________
 *
 * A main class for game BreakOut
 *
 * used classes: board, ball, brick, messages, paddle
 */


package com.shpp.p2p.cs.schernov.assignment4;

import acm.graphics.*;
import com.shpp.cs.a.graphics.WindowProgram;

import java.awt.*;
import java.awt.event.MouseEvent;

public class Breakout extends WindowProgram {
    /**
     * Dimensions of the paddle
     */
    private static final int PADDLE_WIDTH = 60;
    private static final int PADDLE_HEIGHT = 10;

    /**
     * Width and height of application window in pixels
     */
    public static final int APPLICATION_WIDTH = 400 + PADDLE_WIDTH;
    public static final int APPLICATION_HEIGHT = 800;

    /**
     * Dimensions of game board (usually the same)
     */
    private static final int VERTICAL_IDENT = 160;
    private static final int WIDTH = APPLICATION_WIDTH - PADDLE_WIDTH;
    private static final int HEIGHT = APPLICATION_HEIGHT - VERTICAL_IDENT;


    /**
     * Offset of the paddle up from the bottom
     */
    private static final int PADDLE_Y_OFFSET = 30;

    /**
     * Number of bricks per row
     */
    private static final int NBRICKS_PER_ROW = 10;

    /**
     * Number of rows of bricks
     */
    private static final int NBRICK_ROWS = 10;

    /**
     * Separation between bricks
     */
    private static final int BRICK_SEP = 4;

    /**
     * Width of a brick
     */
    private static final double BRICK_WIDTH =
            (WIDTH - (NBRICKS_PER_ROW - 1) * BRICK_SEP) / (NBRICKS_PER_ROW *1.0) - 1.5;

    /**
     * Height of a brick
     */
    private static final int BRICK_HEIGHT = 8;

    /**
     * Radius of the ball in pixels
     */
    private static final int BALL_RADIUS = 10;

    /**
     * Offset of the top brick row from the top
     */
    private static final int BRICK_Y_OFFSET = 70;

    /**
     * Number of turns
     */
    private static final int NTURNS = 3;

    // basic parts of game
    // ball
    Ball ball;
    //board
    Board board;
    //paddle
    Paddle paddle;
    //Game message
    Messages messages;
    //Array of brick
    Brick[][] bricks = new Brick[NBRICK_ROWS][NBRICKS_PER_ROW];
    // XY-Coordinate centered screen
    double centerX, centerY;
    // total bricks count
    int bricksCount = NBRICK_ROWS * NBRICKS_PER_ROW;
    // total score
    int score;
    // total current live
    int currentLive = NTURNS;
    // XY Coordinate for ball
    GPoint pointBall;
    // index of direction
    int indexDirection;

    /**
     * Specifies the program entry point
     */
    public void run() {
        initGame();
    }

    /**
     * The method initGame
     * initialization game:
     * 1. create board
     * 2. create ball
     * 3. create paddle
     * 4. show start message
     */
    private void initGame() {
        // set default data for game
        setSize(APPLICATION_WIDTH, APPLICATION_HEIGHT);
        addMouseListeners(this);

        currentLive = NTURNS;
        score = 0;
        bricksCount = NBRICK_ROWS * NBRICKS_PER_ROW;
        //calculated center
        calcCenter();
        //create ball
        createBall();
        //create paddle
        createPaddle();
        //create board
        createBoard();

    }

    /**
     * The method create a paddle in board
     */
    private void createPaddle() {
        double x = centerX - PADDLE_WIDTH / 2.0;
        double y = HEIGHT - PADDLE_HEIGHT - PADDLE_Y_OFFSET + VERTICAL_IDENT / 2.0;
        paddle = new Paddle(x, y, PADDLE_WIDTH, PADDLE_HEIGHT);
        add(paddle);
    }

    /**
     * the method
     * add mouse listener for move mouse
     * @param e MouseEvent 
     */
    public void mouseMoved(MouseEvent e) {
        //get x coordinate for paddle center cursor
        double x = e.getX()-PADDLE_WIDTH / 2.0;
        //calc min/max X-Coordinate
        if (x - PADDLE_WIDTH /2.0< board.left ) x = board.left;
        if (x+PADDLE_WIDTH > board.right ) x = board.right - PADDLE_WIDTH;
        //set x-Position for paddle
        paddle.setPosition(x);
    }


    /**
     * The method create a game board
     * with borders and info panels
     * Show start messages and run game
     */
    private void createBoard() {
        //create board
        board = new Board(WIDTH, HEIGHT, PADDLE_WIDTH / 2.0, 50);
        currentLive = NTURNS;
        bricksCount = NBRICK_ROWS * NBRICKS_PER_ROW;
        showBorder(board.borders());
        showBorder(board.panels());
        showLabels();
        showLives();
        //create bricks
        buildBricks();
        //show start messages
        showMessage("start");
        showMessage("rule");
        //play game
        waitForClick();
        playGame();
    }

    /**
     * The method create array of bricks
     */
    private void buildBricks() {
        Color color;
        double x, y;
        for (int row = 0; row < NBRICK_ROWS; row++) {
            for (int n = 0; n < NBRICKS_PER_ROW; n++) {
                x = board.left + BRICK_WIDTH * n + BRICK_SEP * (n + 1);
                y = board.top + BRICK_Y_OFFSET + row * BRICK_HEIGHT + row * BRICK_SEP;
                //set color by row position
                color = switch (row / 2) {
                    case 0 -> Color.red;
                    case 1 -> Color.orange;
                    case 2 -> Color.yellow;
                    case 3 -> Color.green;
                    default -> Color.cyan;
                };
                bricks[row][n] = new Brick(x, y, BRICK_WIDTH, BRICK_HEIGHT, color, row, n);
                add(bricks[row][n]);
            }
        }
    }

    /**
     * the method vizualizations
     * current lives for game
     */
    private void showLives() {
        for (GOval live : board.lives(NTURNS, currentLive)) {
            add(live);
        }
    }

    /**
     * the method show all labels in board
     */
    private void showLabels() {
        for (GLabel label : board.labels()) {
            add(label);
        }
        board.scoreLabel.setLabel("Score: " + score);
        board.bricksLabel.setLabel("Bricks: " + bricksCount);
        board.infoLabel.setLabel("For start click mouse button!");
        board.infoLabel.setColor(Color.RED);
    }

    /**
     * The method create vizualization borders in board
     * @param borders array of board in board
     */
    private void showBorder(GRect[] borders) {
        for (GRect border : borders) {
            add(border);
        }
    }

    /**
     * The method create ball in center board
     */
    private void createBall() {
        ball = new Ball(centerX - BALL_RADIUS, centerY - BALL_RADIUS, BALL_RADIUS);
        add(ball);

    }

    /**
     * The method calculated center of screen position
     */
    private void calcCenter() {
        centerX = getWidth() / 2.0;
        centerY = getHeight() / 2.0;
    }

    /**
     * The method playGame
     * started game
     */
    private void playGame() {
        board.infoLabel.setLabel("");
        ball.startMove();
        //main live loop by can play
        while (canPlay()) {
            //check maybe objects
            checkObject();
            //moved ball
            ball.moved();
            //set delay
            pause(10);
        }
        if (bricksCount <=0) {
            //win game
            showMessage("win");
            board.infoLabel.setLabel("Click mouse");
            removeAll();
            initGame();
        }

    }

    /**
     * the method checked maybe objects
     * touched ball
     */
    private void checkObject() {
        //get objects
        GObject object=getObject();
        //skipping null object or ball
        if (object != null && object != ball) {
            if (object == paddle) {
                //ball touch paddle
                kickPaddle();
            }
            else if (object == board.rightBorder || object == board.leftBorder) {
                //ball touch left or right border
                if ((ball.getRightMaxPoint().getX() >= board.rightBorder.getX() && ball.dX > 0) ||
                    (ball.getLeftMaxPoint().getX() <= board.leftBorder.getX()+5 && ball.dX < 0 )){
                        ball.invertDX();
                }
                board.infoLabel.setColor(Color.green);
                board.infoLabel.setLabel("Kick board: new dX - " + ball.dX);
            }
            else if (object == board.topBorder ) {
                //ball touch top border
                ball.invertDY();
                board.infoLabel.setColor(Color.green);
                board.infoLabel.setLabel("Kick board: new dY - " + ball.dY);
            }
            else if (object == board.bottomBorder ) {
                //ball touch bottom border
                loseLive();
            }
            else if (((Brick) object).isBrick) {
                //ball touch brick
                Brick brick = bricks[((Brick) object).row][((Brick) object).column];
                kickBrick(brick);
                ball.invertDY();
            }
            else {
                //else special conditions for bug
                ball.isMoved = false;
                printPosition();
                println(object);
            }
        }
    }

    /**
     * the method describe lose live
     * when ball touch bottom border
     * and decrease current live
     */
    private void loseLive() {
        currentLive --;
        if (currentLive>0){
            //when we can play
            showMessage("lost");
            showLives();
            ball.setLocation(ball.startX, ball.startY);
            board.infoLabel.setColor(Color.RED);
            board.infoLabel.setLabel("Press mouse button");
            waitForClick();
            board.infoLabel.setLabel("");
            ball.startMove();
        }
        else {
            //lose game
            showMessage("lose");
            board.infoLabel.setLabel("Click mouse");
            removeAll();
            initGame();
        }
    }

    /**
     * the method describe kick brick
     * when ball touch one of brick from array
     * set visible brick and sum total points
     * @param brick GRect object
     */
    private void kickBrick(Brick brick) {
        score += brick.kickBrick();
        board.scoreLabel.setLabel("Score: " + score);
        if (brick.hp == 0) {
            remove(brick);
            bricksCount --;
            board.bricksLabel.setLabel("Bricks: "+ bricksCount);
        }
        board.infoLabel.setColor(Color.green);
        board.infoLabel.setLabel("Kick Brick: " + brick.row + " : " + brick.column);
    }

    /**
     * the method describe kick paddle
     * when ball touch paddle
     */
    private void kickPaddle() {
        //get position in paddle when ball touched
        double ballDirectionX = paddle.getPartDirection(ball.getBottomMaxPoint().getX());
        board.infoLabel.setColor(Color.BLUE);
        board.infoLabel.setLabel("Kick paddle! Direction - "+ ballDirectionX);
        //set speed move and reverse Y-Coordinate for ball
        ball.setSpeed(ballDirectionX);
        if (ball.dY > 0) {
            ball.invertDY();
        }
    }

    /**
     * debugging info in console
     */
    private void printPosition() {
        println(pointBall);
        println(indexDirection);
    }

    /** the method getObject returned
     * GObject when ball touch one of max point other object
     *
     * @return GObject
     */
    private GObject getObject() {
        for (int i=0; i<ball.getAllMaxPoints().length; i++){
            GObject object = getElementAt(ball.getAllMaxPoints()[i]);
            if ( object != null && object != ball) {
                pointBall=ball.getAllMaxPoints()[i];
                indexDirection = i;
                return object;
            }
        }
        return null;
    }

    /** The method canPlay
     * descripe possible play in game
     * @return boolean type (True- when ball is moved, count of bricks or live more 0)
     */
    private boolean canPlay() {
        return ball.isMoved && bricksCount > 0 && currentLive > 0;
    }

    /** The method     showMessage
     * show game message by para,
     * @param type String param message (start, rule, lost, lose, win)
     */
    private void showMessage(String type) {
        messages= new Messages(centerX, centerY);
        messages.showMessage(type);
        for (GObject object: messages.objects()) {
            add(object);
        }
        waitForClick();
        hideMessage();

    }

    /**
     * The method hideMessage
     * hidden game messages
     */
    private void hideMessage() {
        for (GObject object: messages.objects()) {
            remove(object);
        }
    }
}
