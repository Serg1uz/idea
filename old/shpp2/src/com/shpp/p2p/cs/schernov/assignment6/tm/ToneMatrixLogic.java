package com.shpp.p2p.cs.schernov.assignment6.tm;

public class ToneMatrixLogic {
    /**
     * Given the contents of the tone matrix, returns a string of notes that should be played
     * to represent that matrix.
     *
     * @param toneMatrix The contents of the tone matrix.
     * @param column     The column number that is currently being played.
     * @param samples    The sound samples associated with each row.
     * @return A sound sample corresponding to all notes currently being played.
     */
    public static double[] matrixToMusic(boolean[][] toneMatrix, int column, double[][] samples) {
        double[] result = new double[ToneMatrixConstants.sampleSize()];
        for (int i = 0; i < toneMatrix.length; i++) {
            if (toneMatrix[i][column]) {
                for (int j = 0; j < samples[i].length; j++) {
                    result[i] += samples[i][j];
                }
            }
        }
        return normalize(result);
    }

    /**
     * The method normalize
     * Normalizes the incoming array to a range [-1, 1]
     *
     * @param arr double[] incoming array
     * @return double[] normalized array
     */
    private static double[] normalize(double[] arr) {
        double max = getMaxElement(arr);
        if (max == 0) return arr;
        for (int i = 0; i < arr.length; i++) arr[i] /= max;
        return arr;
    }


    /**
     * The method getMaxElement
     * Get max element from incoming array
     *
     * @param arr double[] incoming array
     * @return double max element
     */
    private static double getMaxElement(double[] arr) {
        double max = Math.abs(arr[0]);
        for (int i = 1; i < arr.length; i++) max = max >= Math.abs(arr[i]) ? max : Math.abs(arr[i]);
        return max;
    }
}
