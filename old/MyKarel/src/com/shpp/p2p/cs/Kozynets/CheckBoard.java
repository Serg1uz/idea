package com.shpp.kozynets.cs;

import com.shpp.karel.KarelTheRobot;
public class CheckBoard extends KarelTheRobot {

    int count = 0; //create a helper variable

    public void run() throws Exception {
        if (!frontIsClear()) { // check for height, if not, then go wide
            turnLeft();
        }
        passage();
    }
    //override method to put beeper through one
    @Override
    public void move() throws Exception {
        if (count % 2 == 0)
            putBeeper();
        super.move();
        count++;
    }
    //locomotion method
    private void passage() throws Exception {
        while (true) {
            moveLong();
            stepLeft();

            moveLong();
            stepRight();
        }
    }

    private void turnRight() throws Exception {
        for (int i = 0; i < 3; i++) {
            turnLeft();
        }
    }
    private void stepLeft() throws Exception {
        turnLeft();
        move();
        turnLeft();
    }
    private void stepRight() throws Exception {
        turnRight();
        move();
        turnRight();
    }
    // continuous movement method
    private void moveLong() throws Exception {
        while (frontIsClear()) {
            move();
        }
    }
}