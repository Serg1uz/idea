package com.shpp.kozynets.cs;

import com.shpp.karel.KarelTheRobot;

public class StoneMason extends KarelTheRobot {

    public void run() throws Exception {

        turnLeft();
        stepRow(); //A recursive function that does all the work
    }
    //put logic
    private void moveToWall() throws Exception {
        while (frontIsClear()) {
            if (!beepersPresent()) {
                putBeeper();
            }
            move();
            if (!beepersPresent()) {
                putBeeper();
            }
        }
    }
    //
    private void stepRight() throws Exception {
        turnRight();
        move();
        turnLeft();
    }
    private void turnRight() throws Exception {
        for (int i = 0; i < 3; i++) {
            turnLeft();
        }
    }
    private void reverse() throws Exception {
        turnLeft();
        turnLeft();
    }
    //walking logic
    private void stepRow() throws Exception {
        moveToWall();
        if(rightIsClear()) {
            stepRight();
            moveToWall();
        }
        reverse();
        moveToWall();
        reverse();
        stepRight();
        stepRow();
    }
}
