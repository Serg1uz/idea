package com.shpp.kozynets.cs;

import com.shpp.karel.KarelTheRobot;

public class CollectNewspaper extends KarelTheRobot {

    public void run() throws Exception {
        //Go to Second row (step aside)
        stepRight();

        // Let's go until we find a beeper
        moveToBeeper();

        //Find and pick it up, turn around and go to the wall
        pickBeeper();
        reverse();
        moveToWall();

        //Take a step to the side and turn around
        stepRight();
        reverse();

        //Done
    }
    private void turnRight() throws Exception {
        for (int i = 0; i < 3; i++) {
            turnLeft();
        }
    }
    private void reverse() throws Exception {
        turnLeft();
        turnLeft();
    }
    private void moveToBeeper() throws Exception {
        while (noBeepersPresent()) {
            move();
        }
    }
    private void moveToWall() throws Exception {
        while (frontIsClear()) {
            move();
        }
    }
    private void stepRight() throws Exception {
        turnRight();
        move();
        turnLeft();
    }
}