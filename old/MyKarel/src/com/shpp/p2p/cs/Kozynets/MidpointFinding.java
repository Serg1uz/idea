package com.shpp.kozynets.cs;

import com.shpp.karel.KarelTheRobot;

public class MidpointFinding extends KarelTheRobot {

    int count = 0; //counter for length row

    public void run() throws Exception {
        sizeRow();
        reverse();
        pairingChek();
        putBeeper();

    }
    private void pairingChek() throws Exception {
        if(count % 2 == 0) { // pairing check
            for (int i = 0; i < count/2; i++) { // find out what cell we need to become
                move();
            }
        } else {
            for (int i = 0; i < count/2 + 1; i++) {
                move();
            }
        }
    }
    // logic size row
    private void sizeRow() throws Exception {
        while (frontIsClear()) {
            count++;
            move();
        }
    }
    private void reverse() throws Exception {
        turnLeft();
        turnLeft();
    }
}
