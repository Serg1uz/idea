/*
 * File: Assigment1Part2
 *
 * Solves task "Stone Mason" :
 * Karel must fill all columns
 *
 * A simple algorithm for solving the  task:
 * 1. Fill column
 * 2. Skip 4 columns
 * 3. Repeat until not completed all board
 */

package com.shpp.p2p.cs.schernov.assignment1;


public class Assignment1Part2 extends ExtendedKarel {
    /**
     * Specifies the program entry point
     */
    public void run() throws Exception {
        fillAllColumns();
    }

    /**
     * Method fillAllColumns
     * Main method in this solves
     * <p>
     * In the loop until we reach the end, perform the actions
     * 1. fill in the column
     * 2. back to the first row
     * 3. skip four columns
     */
    private void fillAllColumns() throws Exception {
        while (frontIsClear()) {
            fillColumn();
            backToFirstRow();
            skipColumns();
        }
        // fill last column
        fillColumn();
        backToFirstRow();

    }

    /**
     * Method fillColumn
     * <p>
     * loops through all the cells in the column
     * and fills empty cells with beepers
     */
    private void fillColumn() throws Exception {
        turnLeft();
        while (frontIsClear()) {
            if (noBeepersPresent()) {
                putBeeper();
            }
            moveExt();
        }
        // fill last cell
        if (noBeepersPresent()) {
            putBeeper();
        }
    }

    /**
     * Method  backToFirstRow
     * <p>
     * Karel comeback to first cell in row
     * and turn to the left
     **/
    private void backToFirstRow() throws Exception {
        turnAround();
        moveToWall();
        turnLeft();
    }

    /**
     * Method skipColumns
     * <p>
     * Karel takes four steps forward
     */
    private void skipColumns() throws Exception {
        for (int i = 0; i < 4; i++) {
            moveExt();
        }
    }


}
