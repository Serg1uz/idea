/*
 * File: Assigment1Part1
 *
 * Solves task "Collect newspaper" :
 * Karel must leave the house and pick up a newspaper on the doorstep
 *
 * A simple algorithm for solving the  task:
 * 1. Mark start point
 * 2. Found Newspaper
 * 3. Return to start position
 *
 * Rule for the house(board):
 * 1. House(Board) have one door
 * 2. Newspaper one move from the house(board)
 */

package com.shpp.p2p.cs.schernov.assignment1;

public class Assignment1Part1 extends ExtendedKarel {
    /**
     * Specifies the program entry point
     */
    public void run() throws Exception {
        startPoint();
        foundNewspaper();
        backToStart();
    }

    /**
     * Method startPoint
     * mark start position using Beeper
     */
    private void startPoint() throws Exception {
        putBeeper();
    }

    /**
     * Method found of newspaper
     * Using the left hand rule of the maze
     *
     * Before each step, Karel turns to the left
     * and then turns to the right until there are front is blocked
     * move one step.
     * Repeat this actions until not found Beeper
     * After which Karel pick up Beeper
     *
     */
    private void foundNewspaper() throws Exception {
        //get out of start point
        moveExt();
        //search algorithm
        while (noBeepersPresent()) {
            turnLeft();
            while (frontIsBlocked()) {
                turnRight();
            }
            moveExt();
        }
        //pickup
        pickBeeper();
    }


    /**
     * Method returned Karel to start position
     * Using the right hand rule of the maze
     *
     * Before each step, Karel turns to the right
     * and then turns to the left until there are front is blocked
     * move one step.
     * Repeat this actions until not found Beeper
     * After which Karel pick up Beeper on the start point
     */
    private void backToStart() throws Exception {
        //back to home
        turnAround();
        moveExt();
        moveExt();
        //search algorithm
        while (noBeepersPresent()) {
            turnRight();
            while (frontIsBlocked()) {
                turnLeft();
            }
            moveExt();
        }
        //pickup start beepers
        pickBeeper();
    }





}
