/*
 * File: ExtendedKarel
 *
 * Describes a class that extends the basic functionality of the Karel,
 * will be further used as an extends class
 *
 * This class describes the following new features:
 * 1. turn right - method turnRight
 * 2. turn Around - method turnAround
 * 3. take a step if possible - method moveExt
 * 4. move forward to wall - method moveToWall
 * 4. move forward to next Beeper - method moveToBeeper
 */

package com.shpp.p2p.cs.schernov.assignment1;

import com.shpp.karel.KarelTheRobot;

public class ExtendedKarel extends KarelTheRobot {
    /**
     * Method for Karel turn right
     * done by 3 turns to the left
     * used basic method turnLeft()
     */
    public void turnRight() throws Exception {
        for (int i = 0; i < 3; i++) {
            turnLeft();
        }
    }

    /**
     * Method for Karel turn around
     * done by 2 turns to the left
     * used basic method turnLeft()
     */
    public void turnAround() throws Exception {
        for (int i = 0; i < 2; i++) {
            turnLeft();
        }
    }

    /**
     * Method for Karel move one step forward
     * if forward is no wall
     */
    public void moveExt() throws Exception {
        if (frontIsClear()) {
            move();
        }
    }

    /**
     * Method for Karel move forward to wall
     */
    public void moveToWall() throws Exception {
        while (frontIsClear()) {
            move();
        }
    }

    /**
     * Method for Karel move forward to next Beepers
     */
    public void moveToBeeper() throws Exception {
        while (noBeepersPresent()) {
            move();
        }
    }

}
