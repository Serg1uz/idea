/*
 * File: Assigment1Part3
 *
 * Solves task "MidPoint Finding" :
 * Karel must find mid point on the row
 *
 * A simple algorithm for solving the  task:
 * 1. Mark borders in the first and last cell
 * 2. In the loop Karel move from one border to next border and mark new border
 * until not found midpoint
 */

package com.shpp.p2p.cs.schernov.assignment1;

public class Assignment1Part3 extends ExtendedKarel {
    /**
     * Specifies the program entry point
     */
    public void run() throws Exception {
        markFirstBorders();
        foundMidPoint();
    }

    /**
     * Method markFirstBorders
     * defining line boundaries .
     * <p>
     * At the first need check small board 1x1
     * <p>
     * We take a step forward, we lay down the Beeper,
     * we go to the end of the line while we freely turn around,
     * we take a step forward, we lay down the second border (Beeper)
     */
    private void markFirstBorders() throws Exception {
        if (frontIsClear()) {
            putBeeper();
            moveToWall();
            putBeeper();
            turnAround();
            moveExt();
        } else {
            //specific case to world 1x1
            putBeeper();
        }
    }

    /**
     * Method foundMidPoint
     * in the loop until front is clear
     * <p>
     * Karel walks between the beepers,
     * each time decreasing the range by one
     * when there are two beepers next to it,
     * then the previous one was the middle of
     * the line, remove the current one and
     * turn to the wall to exit from the loop
     * <p>
     * specific case check to world 1x2
     */
    private void foundMidPoint() throws Exception {
        if (frontIsClear()) {
            while (frontIsClear()) {
                moveToBeeper();
                markNewBorder();
            }
        } else {
            //specific case
            pickBeeper();
        }
    }

    /**
     * Method markNewBorder
     * decrease the range by moving the beeper to the previous cell
     * and checking the cell in the middle of the row
     */
    private void markNewBorder() throws Exception {
        pickBeeper();
        turnAround();
        moveExt();
        checkMidPoint();
        putBeeper();
        moveExt();
        checkMidPoint();
    }

    /**
     * Method checkMidPoint
     * checked is previous cell is Mid of row
     * <p>
     * if current cell have beeper
     * then previous cell is midpoint
     * pickup current beeper move one step back
     * and turn to the wall (turnRight or turnLeft)
     */
    private void checkMidPoint() throws Exception {
        if (beepersPresent()) {
            pickBeeper();
            turnAround();
            moveExt();
            if (facingEast()) {
                turnRight();
            } else {
                turnLeft();
            }
        }
    }
}
