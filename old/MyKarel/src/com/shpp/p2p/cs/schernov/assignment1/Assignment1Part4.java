/*
 * File: Assigment1Part4
 *
 * Solves task "CheckBoard" :
 * Karel must put beepers in the board by check bord scheme
 *
 * A simple algorithm for solving the  task:
 * Karel move on the board by zigzag scheme and put beepers
 * into odd cell
 */
package com.shpp.p2p.cs.schernov.assignment1;


public class Assignment1Part4 extends ExtendedKarel {
    /**
     * Specifies the program entry point
     */
    public void run() throws Exception {
        //start point
        putBeeper();
        fillBoard();
    }


    /**
     * Method fillBoard
     * Filling board the check board scheme
     * by zigzag algorithm:
     * fill row and get new direction
     * loop until to front is clear
     * <p>
     * Special Case: board with 1 column
     */
    private void fillBoard() throws Exception {
        if (frontIsBlocked()) {
            //special case if board is one column
            turnLeft();
            fillRow();
        } else {
            while (frontIsClear()) {
                fillRow();
                getDirection();
            }
        }
    }

    /**
     * Method fillRow
     * filling row
     * put beepers into odd cell to end of line
     */
    private void fillRow() throws Exception {
        while (frontIsClear()) {
            if (beepersPresent()) {
                moveExt();
            } else {
                moveExt();
                putBeeper();
            }
        }
    }


    /**
     * Method getDirection
     * get directions in the bend of zigzag
     * with check to blocked front
     */
    private void getDirection() throws Exception {
        if (facingEast()) {
            turnLeft();
            if (frontIsClear()) {
                goUp();
                turnLeft();
            }
        } else {
            turnRight();
            if (frontIsClear()) {
                goUp();
                turnRight();
            }
        }
    }

    /**
     * Method goUp
     * get next row and fill the first cell
     * into new row
     */
    private void goUp() throws Exception {
        if (beepersPresent()) {
            moveExt();
        } else {
            moveExt();
            putBeeper();
        }
    }


}
