package com.shpp.p2p.cs.dbotnar.assignment1;

import com.shpp.karel.KarelTheRobot;

public class Assignment1Part2 extends KarelTheRobot {

    /**
     * Precondition: Karel is in the start position.
     * Result: Karel have built rows from stones (1,5,9,etc).
     */
    public void run() throws Exception {
        while (frontIsClear()) {
            buildRowFromStones();
            turnAround();
            backToBottomOfColumn();
            moveToAnotherColumn();
        }

        // Karel build from stones last column.
        if (frontIsBlocked() && leftIsClear()) {
            buildRowFromStones();
        }
    }

    /**
     * Precondition: Karel is in the start position.
     * Result: Karel put one beeper.
     */
    private void buildRowFromStones() throws Exception {
        turnLeft();
        while (frontIsClear()) // Karel perform tasks while front is clear.
        {
            if (noBeepersPresent()) {
                putBeeper();
            }
            move();
        }
        // Karel input beeper in last cell in column.
        if (noBeepersPresent()) {
            putBeeper();
        }
    }

    /**
     * Precondition: non.
     * Result: Karel changed position by 180 degrees.
     */
    private void turnAround() throws Exception {
        turnLeft();
        turnLeft();
    }

    /**
     * Precondition: Karel is in highest position of column.
     * Result: Karel back to bottom of column.
     */
    private void backToBottomOfColumn() throws Exception {
        while (frontIsClear()) {
            move();
        }
    }

    /**
     * Precondition: Karel is in bottom of column.
     * Result: Karel moved to another one column.
     */
    private void moveToAnotherColumn() throws Exception {
        if (leftIsClear()) {
            turnLeft();
            for (int i = 0; i < 4; i++) {
                move();
            }
        }
    }
}

