package com.shpp.p2p.cs.dbotnar.assignment1;

import com.shpp.karel.KarelTheRobot;

public class Assignment1Part1 extends KarelTheRobot {

    /**
     * Precondition:Karel is in start position.
     * Result:Karel take and back newspaper to start position.
     */
    public void run() throws Exception {
        moveToTheBeeper();
        pickBeeper();
        backToStartPosition();
    }

    /**
     * Precondition: Karel is in the start position.
     * Result: Karel is in position for taking beeper.
     */
    private void moveToTheBeeper() throws Exception {
        turnRight();
        move();
        turnLeft();
        while (noBeepersPresent()) // Karel move to the first seen beeper.
        {
            move();
        }
    }

    /**
     * Precondition: non.
     * Result: Karel changed position to right.
     */
    private void turnRight() throws Exception {
        for (int i = 0; i < 3; i++) // Karel turn left three times.
        {
            turnLeft();
        }
    }

    /**
     * Precondition: Karel took beeper.
     * Result: Karel back to start position with beeper.
     */
    private void backToStartPosition() throws Exception {
        turnAround(); // Karel changed position by 180 degrees.
        while (frontIsClear()) // Karel move straight while front is clear.
        {
            move();
        }
        turnRight();
        move();
        turnRight();
    }

    /**
     * Precondition: non.
     * Result: Karel changed position by 180 degrees.
     */
    private void turnAround() throws Exception {
        turnLeft();
        turnLeft();
    }

}
