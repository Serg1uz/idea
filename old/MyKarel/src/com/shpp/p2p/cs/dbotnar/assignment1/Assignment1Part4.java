package com.shpp.p2p.cs.dbotnar.assignment1;

import com.shpp.karel.KarelTheRobot;

public class Assignment1Part4 extends KarelTheRobot {

    /**
     * Precondition: Karel is in start position.
     * Result: Karel built the chessboard.
     */
    public void run() throws Exception {
        if (frontIsBlocked()) {
            putBeeper();
        } else {
            buildingTheChessboard();
        }
    }

    /**
     * Precondition: Karel is in start position.
     * Result: Karel filled every row by beepers in chess style.
     */
    private void buildingTheChessboard() throws Exception {
        RowFilling();
        if (noBeepersPresent()) {
            stepToAnotherRow();
            pairedTypeChess();
        } else {
            stepToAnotherRow();
            oddTypeChess();
        }
    }

    /**
     * Precondition: Karel is in start position.
     * Result: Karel filled one row with first the cell.
     */
    private void RowFilling() throws Exception {
        putBeeper();
        oneRowFilling();
    }

    /**
     * Precondition: Karel filled first cell.
     * Result: Karel filled the all another cells in one row of chessboard.
     */
    private void oneRowFilling() throws Exception {
        while (frontIsClear()) {
            move();
            if (frontIsClear()) {
                move();
                putBeeper();
            }
        }
    }

    /**
     * Precondition: Karel filled one row and ready to step to another one.
     * Result: Karel stepped to nex row.
     */
    private void stepToAnotherRow() throws Exception {
        if (facingWest() && rightIsClear()) {
            turnRight();
            move();
            turnRight();
        } else {
            if (facingEast() && leftIsClear()) {
                turnLeft();
                move();
                turnLeft();
            }
        }
    }

    /**
     * Precondition: non.
     * Result: Karel turned right.
     */
    private void turnRight() throws Exception {
        for (int i = 0; i < 3; i++) {
            turnLeft();
        }
    }

    /**
     * Precondition: Karel filled first row and choose the type of chess filling.
     * Result: Karel filled paired chessboard.
     */
    private void pairedTypeChess() throws Exception {
        while (frontIsClear()) {
            putBeeper();
            move();
            if (frontIsBlocked()) {
                stepToAnotherRow();
            } else {
                move();
            }
        }
    }

    /**
     * Precondition: Karel filled first row and choose the type of chess filling.
     * Result: Karel filled odd chessboard.
     */
    private void oddTypeChess() throws Exception {
        while (frontIsClear()) {
            move();
            putBeeper();
            oneRowFilling();
            stepToAnotherRow();
            RowFilling();
            stepToAnotherRow();
            oddTypeChess();
        }
    }
}