package com.shpp.p2p.cs.dbotnar.assignment1;

import com.shpp.karel.KarelTheRobot;

public class Assignment1Part3 extends KarelTheRobot {

    /**
     * Precondition: Karel is in start position.
     * Result: Karel found the center of south line.
     */
    public void run() throws Exception {
        if (frontIsBlocked()) {
            oneCellFindingCenter();
        } else {
            findingCenterOfSouthLine();
        }
    }

    /**
     * Precondition: Karel is in start position.
     * Result: Karel found the center of south line if 1x1 size of maze.
     */
    private void oneCellFindingCenter() throws Exception {
        putBeeper();
    }

    /**
     * Precondition: Karel is in start position.
     * Result: Karel found center of one line.
     */
    private void findingCenterOfSouthLine() throws Exception {
        сreateLimitersOfLines();  // Karel create limiters of lines using beepers at the end.
        midPointFinding();   // Karel is finding center of line.
    }

    /**
     * Precondition: Karel is in the start position.
     * Result: The lines has been limited by Karel.
     */
    private void сreateLimitersOfLines() throws Exception {
        while (frontIsClear()) {
            move();
        }
        if (noBeepersPresent()) {
            putBeeper();
            turnAround();
            move();
            сreateLimitersOfLines();
        }
    }

    /**
     * Precondition: non.
     * Result: Karel turned around.
     */
    private void turnAround() throws Exception {
        turnLeft();
        turnLeft();
    }

    /**
     * Precondition: Karel has limited lines.
     * Result: Karel found center of the south line.
     */
    private void midPointFinding() throws Exception {
        pickBeeper();
        turnAround();
        move();
        if (noBeepersPresent()) {
            putBeeper();
        }
        while ((frontIsClear())) {
            move();
            if (beepersPresent()) {
                midPointFinding();
            }
        }
    }
}



