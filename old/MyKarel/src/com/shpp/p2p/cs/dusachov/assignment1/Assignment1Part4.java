package com.shpp.p2p.cs.dusachov.assignment1;

public class Assignment1Part4 extends SuperKarel {

    /**
     * Main method for running program
     *
     * @throws Exception
     */
    public void run() throws Exception {

        createChessBoard();
    }

    /**
     * Method for fill chess board
     *
     * @throws Exception
     */
    private void createChessBoard() throws Exception {

        putBeeper();
        fillLine();
        turnLeft();

        while (frontIsClear()) {
            fillLine();
            moveBack();
            turnRight();
            moveToNextColumn();
        }
    }

    /**
     * Fill line in current direction
     *
     * @throws Exception call Exception with any error
     */
    private void fillLine() throws Exception {

        while (frontIsClear()) {
            if(noBeepersPresent()) {
                moveOneStep();
                putBeeper();
            } else {
                moveOneStep();
            }
        }
    }

    /**
     * Move left to next column if front is clear
     *
     * @throws Exception
     */
    private void moveToNextColumn() throws Exception {

        if (frontIsClear()) {
            move();
            turnRight();
        }
    }
}
