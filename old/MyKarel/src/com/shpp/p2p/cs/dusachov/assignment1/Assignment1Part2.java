package com.shpp.p2p.cs.dusachov.assignment1;

public class Assignment1Part2 extends SuperKarel {

    /**
     * Main method for running program
     *
     * @throws Exception
     */
    public void run() throws Exception {

        while (frontIsClear()) {
            buildColumn();
            moveToNextPart();
        }

        buildColumn();
    }

    /**
     * Move to next stage for build column
     *
     * @throws Exception
     */
    private void moveToNextPart() throws Exception {

        for (int i = 0; i < 4; i += 1) {
            moveOneStep();
        }
    }

    /**
     * Check block for beepers and put it on if it doesn't exist
     *
     * @throws Exception
     */
    private void checkBeeper() throws Exception {

        if (noBeepersPresent()) {
            putBeeper();
        }
    }

    /**
     * Build column by beepers
     *
     * @throws Exception
     */
    private void buildColumn() throws Exception {

        turnLeft();

        while (frontIsClear()) {
            checkBeeper();
            move();
        }

        checkBeeper();
        moveBack();
        turnLeft();
    }


}
