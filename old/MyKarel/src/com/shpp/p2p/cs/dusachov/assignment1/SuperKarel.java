package com.shpp.p2p.cs.dusachov.assignment1;

import com.shpp.karel.KarelTheRobot;

public class SuperKarel extends KarelTheRobot {

    /**
     * Turn Karel around himself
     *
     * @throws Exception
     */
    public void turnAround() throws Exception {

        turnLeft();
        turnLeft();
    }

    /**
     * Turn Karel to right
     *
     * @throws Exception
     */
    public void turnRight() throws Exception {

        turnAround();
        turnLeft();
    }

    /**
     * Move forward to the end while front is clear
     *
     * @throws Exception
     */
    public void moveForward() throws Exception {

        while (frontIsClear()) {
            move();
        }
    }

    /**
     * Move forward by a step while front is clear
     *
     * @throws Exception
     */
    public void moveOneStep() throws Exception {

        if (frontIsClear()) {
            move();
        }
    }

    /**
     * Move back to the start
     *
     * @throws Exception
     */
    public void moveBack() throws Exception {

        turnAround();
        moveForward();
    }
}