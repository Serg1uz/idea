package com.shpp.p2p.cs.dusachov.assignment1;

public class Assignment1Part1 extends SuperKarel {

    /**
     * Main method for running program
     *
     * @throws Exception
     */
    public void run() throws Exception {

        leaveTheHouseForNewspaper();
        pickUpNewspaper();
        returnToTheHouse();
    }

    /**
     * Karel goes to newspaper
     *
     * @throws Exception
     */
    private void moveToNewspaper() throws Exception {

        while (noBeepersPresent()) {
            move();
        }
    }

    /**
     * Karel leaves the house for find a newspaper
     *
     * @throws Exception
     */
    private void leaveTheHouseForNewspaper() throws Exception {

        turnRight();
        move();
        turnLeft();
        moveToNewspaper();
    }

    /**
     * Karel picks up a newspaper
     *
     * @throws Exception
     */
    private void pickUpNewspaper() throws Exception {

        if (beepersPresent()) {
            pickBeeper();
        }
    }

    /**
     * Karel return to the house
     *
     * @throws Exception
     */
    private void returnToTheHouse() throws Exception {

        moveBack();
        turnRight();
        move();
        turnRight();
    }

}
