package com.shpp.p2p.cs.dusachov.assignment1;

public class Assignment1Part3 extends SuperKarel {

    /**
     * Main method for running program
     *
     * @throws Exception
     */
    public void run() throws Exception {

        processingBeforeCenterCalculate();

        while (frontIsClear()) {
            move();
            checkBeeper();
        }
    }

    /**
     * Add beepers before start calculate center
     *
     * @throws Exception
     */
    private void processingBeforeCenterCalculate() throws Exception {

        putBeeper();
        moveForward();
        putBeeper();
        turnAround();
    }

    /**
     * Check beepers present for turn around
     *
     * @throws Exception
     */
    private void checkBeeper() throws Exception {

        if (beepersPresent()) {
            pickBeeper();
            turnAround();
            move();
            if (noBeepersPresent()) {
                putBeeper();
            }
        }
    }
}
